/**
 * Resolves a channel from a string, such as an ID, a name, or a mention.
 * @param {string} text - Text to resolve.
 * @param {Collection<Snowflake, Channel>} channels - Collection of channels to find in.
 * @param {boolean} [caseSensitive=false] - Makes finding by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes finding by name match full word only.
 * @returns {Channel}
 */
function resolveChannel(text, channels, caseSensitive = false, wholeWord = false) {
    return channels.get(text) || channels.find(channel => checkChannel(text, channel, caseSensitive, wholeWord));
}

/**
 * Checks if a string could be referring to a channel.
 * @param {string} text - Text to check.
 * @param {Channel} channel - Channel to check.
 * @param {boolean} [caseSensitive=false] - Makes checking by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes checking by name match full word only.
 * @returns {boolean}
 */
function checkChannel(text, channel, caseSensitive = false, wholeWord = false) {
    if (channel.id === text) return true;

    const reg = /<#(\d{17,19})>/;
    const match = text.match(reg);

    if (match && channel.id === match[1]) return true;

    text = caseSensitive ? text : text.toLowerCase();
    const name = caseSensitive ? channel.name : channel.name.toLowerCase();

    if (!wholeWord) {
        return name.includes(text)
        || name.includes(text.replace(/^#/, ''));
    }

    return name === text
    || name === text.replace(/^#/, '');
}

module.exports = resolveChannel;