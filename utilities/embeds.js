const { RichEmbed, Message } = require("discord.js");

/**
 * Send an Error Embed
 * @param {Message} message - The Message that was sent
 * @param {string} [error] - The Message to send
 * @param {boolean} [del=true] - Whether to delete the messages or not
 * @returns {Message}
 */
function error(message, error, del = true) {
    const errorEmbed = new RichEmbed()
        .setAuthor(message.author.tag, message.author.displayAvatarURl)
        .setColor("RED")
        .setDescription(error)
        .setFooter("Pixelmon Generations", "https://cdn.discordapp.com/icons/335569539255500813/7443299bb1b1b9c678e1685875ce8b7c.png")
        .setTimestamp();

    return message.channel.send(errorEmbed).then(msg => {
        if (del) {
            message.delete(15000).catch(() => {});
            msg.delete(15000).catch(() => {});
        }
    });
}

/**
 * Send an Success Embed
 * @param {Message} message - The Message that was sent
 * @param {string} success - The Message to send
 * @param {boolean} [del=true] - Whether to delete the messages or not
 * @returns {Message}
 */
function success(message, success, del = true) {
    const successEmbed = new RichEmbed()
        .setAuthor(message.author.tag, message.author.displayAvatarURl)
        .setColor("GREEN")
        .setDescription(success)
        .setFooter("Pixelmon Generations", "https://cdn.discordapp.com/icons/335569539255500813/7443299bb1b1b9c678e1685875ce8b7c.png")
        .setTimestamp();

    return message.channel.send(successEmbed).then(msg => {
        if (del) {
            message.delete(15000).catch(() => {});
            msg.delete(15000).catch(() => {});
        }
    });
}

module.exports = {
    error,
    success
}