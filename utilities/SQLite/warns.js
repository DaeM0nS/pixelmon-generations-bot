const { User } = require("discord.js");
const { auto } = require("../punish");
const { convert } = require("../time");

module.exports = async (client) => {

    /**
     * Adds a warning to a User
     * @param {Integer} caseID - The ID of the case
     * @param {User} user - The user to warn
     * @param {User} admin - The staff that warned them
     * @param {String} reason - The reason they were warned
     * @returns {Object || null} returns an info object describing any changes made, or null if none were
     */
    client.addWarn = (caseID, user, admin, reason) => {
        if (!(caseID && user && admin && reason)) return null;
        const stmt = client.database.prepare("INSERT INTO warns (caseID, punishedID, adminID, date, reason) VALUES (@caseID, @punishedID, @adminID, @date, @reason);");
        return stmt.run({
            caseID: caseID,
            punishedID: user.id,
            adminID: admin.id,
            date: new Date().getTime(),
            reason: reason
        });
    }

    /**
     * Gets a specific warning
     * @param {Integer} caseID - The ID of the case to search
     * @returns {Object || null} The row of data if found, null otherwise
     */
    client.getWarn = (caseID) => {
        if (!caseID) return null;
        const stmt = client.database.prepare("SELECT * FROM warns WHERE caseID = ?;");
        return stmt.get(caseID);
    }

    /**
     * Gets the amount of warnings a user has.
     * @param {User} user - The User to lookup
     * @returns {Integer} The amount of warnings they have
     */
    client.getWarnCount = (user) => {
        if (!user) return null;
        const stmt = client.database.prepare("SELECT * FROM warns WHERE punishedID = ?;");
        const num = stmt.all(user.id);
        return num.length || 0;
    }

    client.checkWarns = (member) => {
        if (!member) return null;
        const warnCount = client.getWarnCount(member.user);

        if (warnCount > 2) {
            switch (warnCount) {
                case 3:
                    // 5m mute
                    const caseID3 = client.createLog(member.guild, member.user, client.user, "tempmute", JSON.stringify(member.roles.filter(r => r.id !== member.guild.id).map(r => r.id)), "Warning Threshhold: 3 Warnings", convert("5m"));
                    auto.mute.temp(client, caseID3);
                    client.addTempPunish(caseID3, member.user, client.user, "tempmute", convert("5m"), date = new Date().getTime());
                    break;
                case 4:
                    // 30m Mute
                    const caseID4 = client.createLog(member.guild, member.user, client.user, "tempmute", JSON.stringify(member.roles.filter(r => r.id !== member.guild.id).map(r => r.id)), "Warning Threshhold: 4 Warnings", convert("30m"));
                    auto.mute.temp(client, caseID4);
                    client.addTempPunish(caseID4, member.user, client.user, "tempmute", convert("30m"), date = new Date().getTime());
                    break;
                case 5:
                    // Kick
                    auto.kick(client, member, member.guild.members.get(client.user.id), "Warning Threshhold: 5 Warnings");
                    break;
                case 6:
                    // 1d TempBan
                    const caseID6 = client.createLog(member.guild, member.user, client.user, "tempban", null, "Warning Threshhold: 6 Warnings", convert("1d"));
                    auto.ban.temp(client, caseID6);
                    client.addTempPunish(caseID6, member.user, client.user, "tempban", convert("1d"), date = new Date().getTime());
                    break;
                case 7:
                    // PermBan
                    const caseID7 = client.createLog(member.guild, member.user, client.user, "ban", null, "Warning Threshhold: 7 Warnings", null, true);
                    auto.ban.add(client, caseID7);
                    break;
            }
        }
    }

}