const { Guild } = require("discord.js");

module.exports = (client) => {

    /**
     * Modify the GuildSettings
     * @param {Guild} guild - Which guild to modify
     * @param {String} column - Which data to change
     * @param {String} value - What to change the data to
     * @returns {Integar} amount of row changes, null if nothing
     */
    client.updateGuildSettings = (guild, column, value) => {
        if (!(guild && column && value)) return null;
        const stmt = client.database.prepare(`UPDATE PixelGensBot SET ${column} = @value WHERE guildID = @guildID;`);
        return stmt.run({ value: value, guildID: guild.id }).changes;
    }

    /**
     * Return an entire guild's settings
     * @param {Guild} guild - Which guild to lookup
     * @returns {Object} The Guild's Settings, null if nothing found
     */
    client.getGuildSettings = (guild) => {
        if (!guild) return null;
        const stmt = client.database.prepare("SELECT * FROM PixelGensBot WHERE guildID = ?;");
        return stmt.get(guild.id);
    }

    /**
     * Return a specific Guild Setting
     * @param {Guild} guild - Which guild to lookup
     * @param {String} column - Which data to get
     * @returns {String} Requested information, null if nothing found
     */
    client.getGuildSetting = (guild, column) => {
        if (!(guild && column)) return null;
        const stmt = client.database.prepare("SELECT * FROM PixelGensBot WHERE guildID = ?;");
        return stmt.get(guild.id)[column];
    }

    /**
     * Checks if a member is a staff in the Discord
     * @param {GuildMember} member - The Member to lookup
     * @returns {Boolean} Whether they are staff
     */
    client.isStaff = (member) => {
        if (!member) return null;
        const roles = JSON.stringify(client.getGuildSetting(member.guild, "staffRoles"));
        if (member.roles.some(r=> roles.includes(r.id)) || member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) return true;
        else return false
    }

}