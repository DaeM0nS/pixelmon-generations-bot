const { User } = require("discord.js");
const { auto } = require("../punish");

module.exports = async (client) => {
    /**
     * Adda a Temporary Punishment to the table.
     * @param {Integer} caseID - The CaseID for the punishment in the modlogs.
     * @param {User} target - The Targeted User
     * @param {User} staff - The Punishing Staff
     * @param {String} type - The Type of Punishment (Tempban/Tempmute)
     * @param {Integer} length - The Length of the Punishment in Milliseconds
     * @param {Integer} [date=new Date()] - The Date it Ocurred
     * @returns {Object || null} The Result from adding to the table, or null if it wasn't added.
     */
    client.addTempPunish = (caseID, target, staff, type, length, date = new Date().getTime()) => {
        if (!(caseID && target && staff && type && length)) return null;
        if (length < 1 || length == Infinity || isNaN(length)) return null;
        const stmt = client.database.prepare("INSERT INTO tempPunishments (caseID, punishedID, adminID, type, date, length) VALUES (@caseID, @punishedID, @adminID, @type, @date, @length);");
        return stmt.run({
            caseID: caseID,
            punishedID: target.id,
            adminID: staff.id,
            type: type,
            length: length,
            date: date
        });
    }

    /**
     * Checks if a TempPunishment should've been removed but wasn't, then removes them.
     * @returns {Object || null} List of Punishments that weren't removed and re-adds them or null if there are none.
     */
    client.checkTemps = async () => {
        const stmt = client.database.prepare("SELECT * FROM tempPunishments;");
        const all = stmt.all();
        if (all.length < 1) return null;

        const filtered = [];
        await all.forEach(async row => {
            if (row.length < 1) {
                const stmt2 = client.database.prepare("DELETE FROM tempPunishments WHERE caseID = ?;");
                return stmt2.run(caseID);
            } else if ((row.date + row.length) < new Date().getTime()) {
                client.removeTemp(row.caseID);
            } else {
                filtered.push(row);
                switch (row.type) {
                    case "tempmute":
                        auto.mute.temp(client, row.caseID);
                        break;
                    case "tempban":
                        auto.ban.temp(client, row.caseID);
                        break;
                    default:
                        console.log("Why the fuck was this added in here? " + punishment);
                        break;
                }
            }
        });
        return filtered;
    }

    /**
     * Removed a Temporary Punishment that wasn't, but should've been.
     * @param {Integer} CaseID - The Case ID to check.
     * @returns {Object || null} Returns the result from the deleted row, or nothing if nothing was removed.
     */
    client.removeTemp = (caseID) => {
        const stmt = client.database.prepare("SELECT * FROM tempPunishments WHERE caseID=?;");
        const punishment = stmt.get(caseID);
        if (!punishment) return null;
        if ((punishment.date + punishment.length) > new Date().getTime()) return null;
        switch (punishment.type) {
            case "tempmute":
                auto.mute.remove(client, punishment.caseID);
                break;
            case "tempban":
                auto.ban.remove(client, punishment.caseID);
                break;
            default:
                console.log("Why the fuck was this added in here? " + punishment);
                break;
        }
        const stmt2 = client.database.prepare("DELETE FROM tempPunishments WHERE caseID=?;");
        return stmt2.run(caseID);
    }
}