const { Guild, User, Role } = require("discord.js");

module.exports = async(client) => {
    const tempPunishTable = "CREATE TABLE IF NOT EXISTS tempPunishments (" +
    "caseID INTEGER UNIQUE PRIMARY KEY NOT NULL, " +
    "punishedID CHARACTER(64) NOT NULL, " +
    "adminID CHARACTER(64) NOT NULL, " +
    "type STRING NOT NULL, " +
    "length INTEGER DEFAULT -1, " + 
    "date INTEGER NOT NULL);";

    const tempPunishIndex = "CREATE UNIQUE INDEX IF NOT EXISTS tempPunishments_log_id_uindex ON tempPunishments (caseID);";

    client.database.prepare(tempPunishTable).run();
    client.database.prepare(tempPunishIndex).run();
}