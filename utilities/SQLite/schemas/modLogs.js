module.exports = async(client) => {
    const punishTable = "CREATE TABLE IF NOT EXISTS punishments (" +
        "caseID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
        "guildID CHARACTER(18) NOT NULL, " +
        "punishedID CHARACTER(64) NOT NULL, " +
        "staffID CHARACTER(64) NOT NULL, " +
        "roles TEXT, " +
        "type STRING NOT NULL, " +
        "date INTEGER NOT NULL, " + 
        "reason STRING, " +
        "length INTEGER, " +
        "expired INTEGER DEFAULT 0 NOT NULL);";

    const punishIndex = "CREATE UNIQUE INDEX IF NOT EXISTS punishments_log_id_uindex ON  punishments (caseID);";

    client.database.prepare(punishTable).run();
    client.database.prepare(punishIndex).run();
}