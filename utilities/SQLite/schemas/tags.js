module.exports = async(client) => {
    const tagsTable = "CREATE TABLE IF NOT EXISTS tags (" +
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
        "guildID STRING NOT NULL, " +
        "name STRING NOT NULL, " +
        "description STRING, " + 
        "category STRING DEFAULT null, " +
        "embed INTEGER DEFAULT 0, " +
        "dm INTEGER DEFAULT 0, " +
        "addedBy CHARACTER(20) NOT NULL, " +
        "addedAt INTEGER NOT NULL, " +
        "modifiedBy CHARACTER(20) DEFAULT NULL, " + 
        "modifiedAt INTEGER DEFAULT NULL, " + 
        "uses INTEGER DEFAULT 0 NOT NULL, " + 
        "aliases STRING);";

    const tagsIndex = "CREATE UNIQUE INDEX IF NOT EXISTS tags_log_id_uindex ON tags (name);";

    client.database.prepare(tagsTable).run();
    client.database.prepare(tagsIndex).run();
}