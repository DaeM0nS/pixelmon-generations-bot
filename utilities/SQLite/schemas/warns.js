module.exports = async(client) => {    
    const warnTable = "CREATE TABLE IF NOT EXISTS warns (" + 
    "caseID INTEGER PRIMARY KEY NOT NULL, " + 
    "punishedID STRING NOT NULL, " + 
    "adminID CHARACTER(64) NOT NULL, " +
    "date INTEGER NOT NULL, " + 
    "reason STRING);"

    const warnIndex = "CREATE UNIQUE INDEX IF NOT EXISTS warns_log_id_uindex ON warns (caseID);";
    
    client.database.prepare(warnTable).run();
    client.database.prepare(warnIndex).run();
}