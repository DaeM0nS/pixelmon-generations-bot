module.exports = async(client) => {
    const mainTable = "CREATE TABLE IF NOT EXISTS PixelGensBot (" +
        "guildID CHARACTER(20) PRIMARY KEY NOT NULL, " +
        "staffRoles STRING DEFAULT '[]', " +
        "ignoredChannels STRING DEFAULT '[]', " +
        "welcomeChannel CHARACTER(20), " +
        "welcomeMessage TEXT, " +
        "leaveChannel CHARACTER(20), " +
        "leaveMessage TEXT, " +
        "punishLog CHARACTER(20)," +
        "chatLogsChannel CHARACTER(20));";

    const mainIndex = "CREATE UNIQUE INDEX IF NOT EXISTS PixelGensBot_log_id_uindex ON PixelGensBot (guildID);";

    client.database.prepare(mainTable).run();
    client.database.prepare(mainIndex).run();
}