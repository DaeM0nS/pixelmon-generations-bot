const SQLite = require("better-sqlite3");
let db = new SQLite("./database/data.db");

module.exports = client => {
    if (!db.open) {
        db = null;
        db = new SQLite("./database/data.db");
    }
    db.pragma("synchronous = 1");
    db.pragma("journal_mode = wal");
    db.pragma("cache_size = 32000");
    return db;
}