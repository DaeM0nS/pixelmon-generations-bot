const { Guild, User, Role } = require("discord.js");

module.exports = (client) => {
    
    /**
     * Creates a ModLog in the Database
     * @param {Guild} guild - Guild it ocurred in
     * @param {User} punished - The Punished User
     * @param {User} staff - The Staff that Punished
     * @param {String} punishment - What they were punished with
     * @param {Collection<Snowflake, Role>} roles - The Roles the Member had (For Mutes)
     * @param {String} reason - The Reason they were punished
     * @param {integar} length - The Length of the punishment (-1 for infinite)
     * @param {Boolean} [expired = false] - Whether the Punishment is expired (instant expires for bans/mutes)
     * @returns {Integar} lastInsertRowid
     */
    client.createLog = (guild, punished, staff, punishment, roles, reason, length, expired = false) => {
        let modLog = {
            guildID: guild.id,
            punishedID: punished.id,
            staffID: staff.id,
            roles: roles,
            type: punishment.toLowerCase(),
            date: Date.now(),
            reason: reason,
            length: length > 0 ? length : -1,
            expired: expired ? 1 : 0
        }

        return client.database.prepare("INSERT INTO punishments (guildID, punishedID, staffID, roles, type, date, reason, length, expired) VALUES (@guildID, @punishedID, @staffID, @roles, @type, @date, @reason, @length, @expired);").run(modLog).lastInsertRowid;
    }

    /**
     * Gets a Punishment based off a User and their Punishment
     * @param {User} user - The Punished User to lookup
     * @param {String} punishment - The Punishment to lookup
     * @returns {Object} - Object of the Punishment Information, or Null
     */
    client.getPunishment = (user, punishment) => {
        if (!(user && punishment)) return null;
        const stmt = client.database.prepare('SELECT * FROM punishments WHERE (punishedID = ? AND type = ?);');
        return stmt.get(user.id, punishment);
    }

    /**
     * Gets a Punishment based off a User and their Punishment
     * @param {User} user - The Punished User to lookup
     * @param {String} punishment - The Punishment to lookup
     * @returns {Object} - Object of the Punishment Information, or Null
     */
    client.getPunishments = (user, punishment) => {
        if (!(user && punishment)) return null;
        const stmt = client.database.prepare('SELECT * FROM punishments WHERE (punishedID = ? AND type = ?);');
        return stmt.all(user.id, punishment);
    }

    /**
     * Gets a Punishment based off a User and their Punishment
     * @param {User} user - The Punished User to lookup
     * @param {String} punishment - The Punishment to lookup
     * @returns {Object} - Object of the Punishment Information, or Null
     */
    client.getUnexpiredPunishment = (user, punishment) => {
        if (!(user && punishment)) return null;
        const stmt = client.database.prepare('SELECT * FROM punishments WHERE (punishedID = ? AND type = ? AND expired = 0);');
        return stmt.get(user.id, punishment);
    }

    client.getUnknownPunishment = user => {
        if (!user) return null;
        const stmt = client.database.prepare("SELECT * FROM punishments WHERE (punishedID = ?);");
        const all = stmt.all(user.id);
        return all ? all[all.length - 1] : all;
    }

    client.getUnknownUnexpiredPunishment = user => {
        if (!user) return null;
        const stmt = client.database.prepare('SELECT * FROM punishments WHERE (punishedID = ? AND expired = 0);');
        const all = stmt.all(user.id);
        return all ? all[all.length - 1] : all
    }

    /**
     * Gets a Punishment based off of a case ID
     * @param {Integar} caseID - The Punished User to lookup
     * @returns {Object} - Object of the Punishment Information, or Null
     */
    client.getCase = (caseID) => {
        if (!caseID) return null;
        const stmt = client.database.prepare('SELECT * FROM punishments WHERE caseID = ?;');
        return stmt.get(caseID);
    }
    
    /**
     * Returns every punishment a user has had.
     * @param {User} user - The Punished User to lookup
     * @returns {Object} - Object of the Punishment Information, or Null
     */
    client.getUserPunishments = (user) => {
        if (!user) return null;
        const stmt = client.database.prepare("SELECT * FROM punishments WHERE punishedID = ?;");
        return stmt.all(user.id);
    }

    /**
     * Expires a Punishment
     * @param {Integar} caseID - The CaseID to expire
     * @returns {null}
     */
    client.expirePunishment = (caseID) => {
        if (!client.getCase(caseID)) return null;
        const stmt = client.database.prepare('UPDATE punishments SET expired = 1 WHERE caseID = ?;');
        return stmt.run(caseID);
    }
}