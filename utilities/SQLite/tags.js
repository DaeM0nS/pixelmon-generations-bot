module.exports = async (client) => {
    
    client.createTag = (guild, name, message, author, embed, category, aliases) => {
        if (client.database.prepare("SELECT * FROM tags WHERE NAME = ? AND guildId = ?;").get(name, guild.id) || client.commands.has(name) || client.aliases.has(name)) return null;

        const stmt = client.database.prepare("INSERT INTO tags (guildID, name, description, category, embed, addedBy, addedAt, aliases) VALUES (@guildID, @name, @description, @category, @embed, @addedBy, @addedAt, @aliases);");
        return stmt.run({
            guildID: guild.id,
            name: name.toLowerCase(),
            description: message,
            category, category,
            embed: embed ? 1 : 0,
            addedBy: author.id,
            addedAt: new Date().getTime(),
            aliases: aliases || JSON.stringify([])
        });
    }

    client.getTag = (guild, name) => {
        if (!(guild && name)) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE name = ? AND guildID = ?;");
        return stmt.get(name.toLowerCase(), guild.id);
    }

    client.getTagByAlias = (guild, alias) => {
        if (!(guild && alias)) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE aliases LIKE ? AND guildID = ?;");
        return stmt.get(`%"${alias.toLowerCase()}"%`, guild.id);
    }

    client.getAllTags = (guild) => {
        if (!guild) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE guildID = ?;");
        const tags = stmt.all(guild.id);
        return tags;
    }

    client.addAlias = (guild, name, alias) => {
        if (!(guild && name)) return null;
        const tag = client.getTag(guild, name);
        let aliases = JSON.parse(tag.aliases);
        aliases.push(alias.toLowerCase());
        const stmt = client.database.prepare("UPDATE tags SET aliases = ? WHERE id = ?;");
        return stmt.run(JSON.stringify(aliases), tag.id);
    }

    client.tagCategory = (guild, name, category) => {
        if (!(guild && name && category)) return null;
        const tag = client.getTag(guild, name);
        if (!tag) return null;
        const stmt = client.database.prepare("UPDATE tags SET category = ? WHERE id = ?;");
        return stmt.run(category.toLowerCase(), tag.id);
    }

    client.tagEmbed = (guild, name, embed) => {
        if (!(guild && name && embed)) return null;
        const tag = client.getTag(guild, name);
        if (!tag) return null;
        const stmt = client.database.prepare("UPDATE tags SET embed = ? WHERE id = ?;");
        return stmt.run(embed ? 1 : 0, tag.id);
    }

    client.updateTag = (guild, name, message) => {
        if (!(guild && name)) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE name = ? AND guildID = ?;");
        const tag = stmt.get(name, guild.id);
        if (tag && message) {
            const stmt2 = client.database.prepare("UPDATE tags SET description = ? WHERE id = ?;");
            return stmt2.run(message, tag.id);
        } else return null
    }

    client.renameTag = (guild, oldName, newName, staff) => {
        if (!(guild && oldName && newName)) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE name = ? AND guildID = ?;");
        const tag = stmt.get(oldName, guild.id);
        if (tag) {
            const stmt2 = client.database.prepare("UPDATE tags SET name = @name, modifiedBy = @modifiedBy, modifiedAt = @modifiedAt WHERE id = @id;");
            return stmt2.run({
                name: newName.toLowerCase(),
                modifiedBy: staff.id,
                modifiedAt: new Date().getTime(),
                id: tag.id
            });
        }
        return null;
    }

    client.deleteTag = (guild, name) => {
        if (!(guild && name)) return null;
        const stmt = client.database.prepare("DELETE FROM tags WHERE name = ? AND guildID = ?;");
        return stmt.run(name, guild.id);
    }

    client.incrementTagUsage = (guild, name) => {
        if (!(guild && name)) return null;
        const stmt = client.database.prepare("SELECT * FROM tags WHERE name = ? AND guildID = ?;");
        const tag = stmt.get(name, guild.id);
        if (!tag) return;
        const stmt2 = client.database.prepare("UPDATE tags SET uses = @uses WHERE id = @id;");
        return stmt2.run({ id: tag.id, uses: tag.uses+1 });
    }

}