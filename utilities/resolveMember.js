const { Collection, Snowflake, GuildMember } = require("discord.js");

/**
 * Resolves a member from a string, such as an ID, a name, or a mention.
 * @param {string} text - Text to resolve.
 * @param {Collection<Snowflake, GuildMember>} members - Collection of members to find in.
 * @param {boolean} [caseSensitive=false] - Makes finding by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes finding by name match full word only.
 * @returns {GuildMember}
 */
function resolveMember(text, members, caseSensitive = false, wholeWord = false) {
    return members.get(text) || members.find(member => checkMember(text, member, caseSensitive, wholeWord));
}

/**
 * Checks if a string could be referring to a member.
 * @param {string} text - Text to check.
 * @param {GuildMember} member - Member to check.
 * @param {boolean} [caseSensitive=false] - Makes checking by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes checking by name match full word only.
 * @returns {boolean}
 */
function checkMember(text, member, caseSensitive = false, wholeWord = false) {
    if (member.id === text) return true;

    const reg = /<@!?(\d{17,19})>/;
    const match = text.match(reg);

    if (match && member.id === match[1]) return true;

    text = caseSensitive ? text : text.toLowerCase();
    const username = caseSensitive ? member.user.username : member.user.username.toLowerCase();
    const displayName = caseSensitive ? member.displayName : member.displayName.toLowerCase();
    const discrim = member.user.discriminator;

    if (!wholeWord) {
        return displayName.includes(text)
        || username.includes(text)
        || ((username.includes(text.split('#')[0]) || displayName.includes(text.split('#')[0])) && discrim.includes(text.split('#')[1]));
    }

    return displayName === text
    || username === text
    || ((username === text.split('#')[0] || displayName === text.split('#')[0]) && discrim === text.split('#')[1]);
}

module.exports = resolveMember;