const { Collection, Snowflake, Role } = require("discord.js");

/**
 * Resolves a role from a string, such as an ID, a name, or a mention.
 * @param {string} text - Text to resolve.
 * @param {Collection<Snowflake, Role>} roles - Collection of roles to find in.
 * @param {boolean} [caseSensitive=false] - Makes finding by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes finding by name match full word only.
 * @returns {Role}
 */
function resolveRole(text, roles, caseSensitive = false, wholeWord = false) {
    return roles.get(text) || roles.find(role => checkRole(text, role, caseSensitive, wholeWord));
}

/**
 * Resolves multiple roles from a string, such as an ID, a name, or a mention.
 * @param {string} text - Text to resolve.
 * @param {Collection<Snowflake, Role>} roles - Collection of roles to find in.
 * @param {boolean} [caseSensitive=false] - Makes finding by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes finding by name match full word only.
 * @returns {Collection<Snowflake, Role>}
 */
function resolveRoles(text, roles, caseSensitive = false, wholeWord = false) {
    return roles.filter(role => checkRole(text, role, caseSensitive, wholeWord));
}

/**
 * Checks if a string could be referring to a role.
 * @param {string} text - Text to check.
 * @param {Role} role - Role to check.
 * @param {boolean} [caseSensitive=false] - Makes checking by name case sensitive.
 * @param {boolean} [wholeWord=false] - Makes checking by name match full word only.
 * @returns {boolean}
 */
function checkRole(text, role, caseSensitive = false, wholeWord = false) {
    if (role.id === text) return true;

    const reg = /<@&(\d{17,19})>/;
    const match = text.match(reg);

    if (match && role.id === match[1]) return true;

    text = caseSensitive ? text : text.toLowerCase();
    const name = caseSensitive ? role.name : role.name.toLowerCase();

    if (!wholeWord) {
        return name.includes(text) || name.includes(text.replace(/^@/, ''));
    }

    return name === text || name === text.replace(/^@/, '');
}

module.exports = {
    resolveRole,
    resolveRoles
}