const { RichEmbed, Client, GuildMember, User, Message } = require("discord.js");
const { convert } = require("../utilities/time");

module.exports = {
    /**
     * Warns a GuildMember and adds the punishment to the Database
     * @param {Client} client - The Instance of the bot
     * @param {Message} message - The message that ran the command
     * @param {GuildMember} target - The GuildMember being punished
     * @param {String} [reason] - Optional reason
     * @returns {null}
     */
    warn: async (client, message, target, reason) => {
        const caseID = client.createLog(message.guild, target, message.member, "warn", null, reason, null, true);
        client.addWarn(caseID, target.user, message.author, reason);
        client.checkWarns(target);

        const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
        let punishChannel;
        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

        let msg = `Successfully warned that member for "${reason}"...`;
        target.send(`You were warned in ${message.guild.name} for "${reason}"`).catch(() => {
            msg += "\nBut their DMs were closed, so I sent it in here.";
            message.channel.send(`${target}, you were warned for "${reason}".`);
        });

        message.channel.send(new RichEmbed().setDescription(msg).setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

        if (!punishChannel) return;
        const warnCount = client.getWarnCount(target.user);
        const warnEmbed = new RichEmbed()
            .setAuthor(`Moderation: Warn [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] ${target.user.tag}`, target.user.displayAvatarURL)
            .setColor("BLUE")
            .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** ${reason}\n**Warn Count:** ${warnCount}`)
            .setFooter(client.user.username, client.user.displayAvatarURL)
            .setTimestamp();

        return punishChannel.send(warnEmbed);
    },
    /**
     * Kicks a GuildMember and adds the punishment to the Database
     * @param {Client} client - The Instance of the bot
     * @param {Message} message - The message that ran the command
     * @param {GuildMember} target - The GuildMember being punished
     * @param {String} [reason] - Optional reason
     * @returns {null}
     */
    kick: async (client, message, target, reason) => {
        if (!reason) reason = "None";

        const caseID = client.createLog(message.guild, target, message.member, "kick", null, reason, null, true);

        target.kick(reason).then(async () => {
            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            message.channel.send(new RichEmbed().setDescription("Successfully kicked that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

            if (!punishChannel) return;
            const kickEmbed = new RichEmbed()
                .setAuthor(`Moderation: Kick [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] ${target.user.tag}`, target.user.displayAvatarURL)
                .setColor("BLUE")
                .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** ${reason}`)
                .setFooter(client.user.username, client.user.displayAvatarURL)
                .setTimestamp();

            return punishChannel.send(kickEmbed);
        }).catch(err => message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {})));
    },
    mute: {
        /**
         * Unmutes a User in the guild and expires the punishment, if they have an unexpired one in the database.
         * @param {Client} client - The Instance of the bot
         * @param {User || Integer} target_or_caseID - The User being punished, or the Case ID to lookup
         * @param {String} type - The Punishment Type (Mute or Tempmute)
         * @returns {Boolean} Did it succeed?
         */
        remove: async (client, target_or_caseID, type) => {
            let punishment;

            if (target_or_caseID instanceof User) {
                if (type === "mute") {
                    punishment = client.getPunishments(target_or_caseID, type);
                    if (typeof punishment === "object") punishment = punishment.reduce((prev, current) => (prev.startdate > current.startdate) ? prev : current);
                } else if (type === "tempmute") {
                    punishment = client.getUnexpiredPunishment(target_or_caseID, type);
                } else if (type === "unknown") {
                    punishment = client.getUnknownUnexpiredPunishment(target_or_caseID, type);
                    if (!punishment || punishment.length > 1) punishment = client.getUnknownPunishment(target_or_caseID, type);
                } else {
                    return false;
                }
            } else punishment = client.getCase(target_or_caseID);
            if (!punishment) return false;

            const roles = JSON.parse(punishment.roles);

            const guild = client.guilds.get(punishment.guildID);
            const tMember = guild.members.get(punishment.punishedID);
            if (!tMember) return;

            await tMember.setRoles(roles).then(async () => {
                if (!punishment.expired) client.expirePunishment(punishment.caseID);
                client.removeTemp(caseID);
                return punishment;
            }).catch(() => { return false; });
        },
        /**
         * Temporarily Mutes a User from the guild and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {Message} message - The message that ran the command
         * @param {User} target - The User being punished
         * @param {Integer} length - The Length of the ban in Milliseconds
         * @param {String} reason - The Reason for the ban
         * @returns {null}
         */
        temp: async (client, message, target, length, reason) => {
            const muteRule = message.guild.roles.find(r => r.name.toLowerCase().includes("mute"));
            if (!muteRule) return;

            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            let tMember = message.guild.members.get(target.id);
            if (!tMember) return;

            const roles = JSON.stringify(tMember.roles.filter(r => r.id !== tMember.guild.id).map(r => r.id));

            tMember.setRoles([muteRule.id], reason).then(async () => {

                const caseID = client.createLog(message.guild, target, message.author, "tempmute", roles, reason, length, false);
                client.addTempPunish(caseID, target, message.author, "tempmute", length);

                setTimeout(async () => {
                    if (client.status === 5) return;
                    if ([4, 2].includes(client.status)) {
                        setTimeout(async () => {
                            if (![0, 1, 3].includes(client.status)) return;
                            await module.exports.mute.remove(client, caseID, "tempmute");
                            if (punishChannel) {
                                const unmuteEmbed = new RichEmbed()
                                    .setAuthor(`Moderation: TempMute Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                    .setColor("BLUE")
                                    .setDescription(`**Punished By:** ${client.user}} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** TempMute Expired for "${reason}"`)
                                    .setFooter(client.user.username, client.user.displayAvatarURL)
                                    .setTimestamp();

                                punishChannel.send(unmuteEmbed);
                            }
                        }, 60000);
                    } else {
                        await module.exports.mute.remove(client, caseID, "tempmute");
                        if (punishChannel) {
                            const unmuteEmbed = new RichEmbed()
                                .setAuthor(`Moderation: TempMute Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                .setColor("BLUE")
                                .setDescription(`**Punished By:** ${client.user}} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** TempMute Expired for "${reason}"`)
                                .setFooter(client.user.username, client.user.displayAvatarURL)
                                .setTimestamp();

                            punishChannel.send(unmuteEmbed);
                        }
                    }
                }, length);

                message.channel.send(new RichEmbed().setDescription("Successfully muted that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));
                if (!punishChannel) return;
                const muteEmbed = new RichEmbed()
                    .setAuthor(`Moderation: TempMute [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Length:** ${convert(length, { long: true })}\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(muteEmbed);
            }).catch(async (err) => {
                return message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));
            });
        },
        /**
         * Mute a User from the guild and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {Message} message - The message that ran the command
         * @param {User} target - The User being punished
         * @param {String} reason - The Reason for the ban
         * @returns {null}
         */
        add: async (client, message, target, reason) => {
            const muteRule = message.guild.roles.find(r => r.name.toLowerCase().includes("mute"));
            if (!muteRule) return;

            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);
            let tMember = message.guild.members.get(target.id);
            if (!tMember) return;

            let roles = JSON.stringify(tMember.roles.filter(r => r.id !== tMember.guild.id));

            tMember.setRoles([muteRule.id], reason).then(async () => {
                const caseID = client.createLog(message.guild, target, message.author, "mute", roles, reason, null, false);

                message.channel.send(new RichEmbed().setDescription("Successfully muted that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

                if (!punishChannel) return;
                const muteEmbed = new RichEmbed()
                    .setAuthor(`Moderation: Mute [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(muteEmbed);
            }).catch(async (err) => {
                return message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));
            });
        }
    },
    ban: {
        /**
         * Unbans a User in the guild and expires the punishment, if they have an unexpired one in the database.
         * @param {Client} client - The Instance of the bot
         * @param {User || Integer} target_or_caseID - The User being punished, or the Case ID to lookup
         * @param {String} type - The Punishment Type (Ban or Tempban)
         * @returns {Boolean} Did it succeed?
         */
        remove: async (client, target_or_caseID, type) => {
            let punishment = false;

            if (target_or_caseID instanceof User) {
                if (type === "ban") {
                    punishment = client.getPunishments(target_or_caseID, type)
                    if (typeof punishment === "object") punishment = punishment.reduce((prev, current) => (prev.startdate > current.startdate) ? prev : current);
                } else if (type === "tempban") {
                    punishment = client.getUnexpiredPunishment(target_or_caseID, type);
                } else if (type === "unknown") {
                    punishment = client.getUnknownUnexpiredPunishment(target_or_caseID);
                    if (!punishment || punishment.length < 1) punishment = client.getUnknownPunishment(target_or_caseID);
                } else {
                    return punishment;
                }
            } else punishment = client.getCase(target_or_caseID);

            if (!punishment) return punishment;

            const guild = client.guilds.get(punishment.guildID);
            await guild.fetchBan(punishment.punishedID).then(async ban => {
                await guild.unban(ban.user).then(() => {
                    if (!punishment.expired) client.expirePunishment(punishment.caseID);
                }).catch((err) => console.log(err));
            }).catch((err) => console.log(err));
            return punishment;
        },
        /**
         * Temporarily Ban a User from the guild and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {Message} message - The message that ran the command
         * @param {User} target - The User being punished
         * @param {Integer} length - The Length of the ban in Milliseconds
         * @param {String} reason - The Reason for the ban
         * @returns {null}
         */
        temp: async (client, message, target, length, reason) => {
            if (length < 1 || length == Infinity) return module.exports.ban.add(client, message, target, reason);

            const caseID = client.createLog(message.guild, target, message.author, "tempban", null, reason, length, false);
            client.addTempPunish(caseID, target, message.author, "tempban", length);

            message.guild.ban(target, { reason: `tempban: \"${reason}\" by ${message.author.tag} (${message.author.id})` }).then(async () => {

                const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
                let punishChannel;
                if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                setTimeout(async () => {
                    if (client.status === 5) return;
                    if ([4, 2].includes(client.status)) {
                        setTimeout(async () => {
                            if (![0, 1, 3].includes(client.status)) return;
                            if (punishChannel) {
                                const unbanEmbed = new RichEmbed()
                                    .setAuthor(`Moderation: TempBan Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                    .setColor("BLUE")
                                    .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** Tempban Expired for "${reason}"`)
                                    .setFooter(client.user.username, client.user.displayAvatarURL)
                                    .setTimestamp();

                                punishChannel.send(unbanEmbed);
                            }
                            return await module.exports.ban.remove(client, caseID, "tempban");
                        }, length);
                    } else {
                        if (punishChannel) {
                            const unbanEmbed = new RichEmbed()
                                .setAuthor(`Moderation: TempBan Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                .setColor("BLUE")
                                .setDescription(`**Punished By:** ${client.user}} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** Tempban Expired for "${reason}"`)
                                .setFooter(client.user.username, client.user.displayAvatarURL)
                                .setTimestamp();

                            punishChannel.send(unbanEmbed);
                        }
                        return await module.exports.ban.remove(client, caseID, "tempban");
                    }
                }, length);

                message.channel.send(new RichEmbed().setDescription("Successfully banned that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

                if (!punishChannel) return;
                const hackbanEmbed = new RichEmbed()
                    .setAuthor(`Moderation: TempBan [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Length:** ${convert(length, { long: true })}\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(hackbanEmbed);
            }).catch(err => {
                console.log(err);
                message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));
            });
        },
        /**
         * Ban a User from the guild and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {Message} message - The message that ran the command
         * @param {User} target - The User being punished
         * @param {String} reason - The Reason for the ban
         * @returns {null}
         */
        add: async (client, message, target, reason) => {
            const caseID = client.createLog(message.guild, target, message.author, "ban", null, reason, null, true);

            message.guild.ban(target, { reason: `ban: \"${reason}\" by ${message.author.tag} (${message.author.id})` }).then(async () => {
                const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
                let punishChannel;
                if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                message.channel.send(new RichEmbed().setDescription("Successfully banned that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

                if (!punishChannel) return;
                const banEmbed = new RichEmbed()
                    .setAuthor(`Moderation: Ban [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(banEmbed);
            }).catch(err => message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {})));
        },
        /**
         * Ban a User before they join the guild and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {Message} message - The message that ran the command
         * @param {User} target - The User being punished
         * @param {String} reason - The Reason for the ban
         * @returns {null}
         */
        hack: async (client, message, target, reason) => {

            const caseID = client.createLog(message.guild, target, message.author, "ban", null, reason, null, true);

            message.guild.ban(target, { reason: `Hackban: \"${reason}\" by ${message.author.tag} (${message.author.id})` }).then(async () => {
                const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
                let punishChannel;
                if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                message.channel.send(new RichEmbed().setDescription("Successfully banned that member...").setColor("GREEN").setTimestamp()).then(msg => msg.delete(15000).catch(() => {}));

                if (!punishChannel) return;
                const hackbanEmbed = new RichEmbed()
                    .setAuthor(`Moderation: HackBan [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(hackbanEmbed);
            }).catch(err => message.channel.send(new RichEmbed().setTitle("Error").setDescription(`\`${err.message}\``).setColor("RED").setTimestamp()).then(msg => msg.delete(15000).catch(() => {})));
        }
    },
    auto: {
        /**
         * Kicks a GuildMember and adds the punishment to the Database
         * @param {Client} client - The Instance of the bot
         * @param {GuildMember} target - The GuildMember being punished
         * @param {GuildMember} staff - The Staff that punished them
         * @param {String} [reason] - Optional reason
         * @returns {null}
         */
        kick: async (client, target, staff, reason) => {
            const guild = target.guild;

            if (!reason) reason = "None";

            const caseID = client.createLog(guild, target, staff, "kick", null, reason, null, true);

            target.kick(reason).then(async () => {
                const punishChannelID = client.getGuildSetting(guild, "punishLog");
                let punishChannel;
                if (punishChannelID) punishChannel = guild.channels.get(punishChannelID);

                if (!punishChannel) return;
                const kickEmbed = new RichEmbed()
                    .setAuthor(`Moderation: Kick [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${staff.user} (${staff.user.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                return punishChannel.send(kickEmbed);
            }).catch(() => {});
        },
        mute: {
            /**
             * Unmutes a User in the guild and expires the punishment.
             * @param {Client} client - The Instance of the bot
             * @param {User || Integer} caseID - The Case ID to lookup
             * @returns {Boolean} Did it succeed?
             */
            remove: async (client, caseID) => {
                const punishment = client.getCase(caseID);
                if (!punishment) return false;

                const guild = client.guilds.get(punishment.guildID);

                const tMember = guild.members.get(punishment.punishedID);
                if (!tMember) return;

                const roles = JSON.parse(punishment.roles);

                tMember.setRoles(roles).then(async () => {
                    if (!punishment.expired) client.expirePunishment(caseID);
                    client.removeTemp(caseID);

                    const punishChannelID = client.getGuildSetting(guild, "punishLog");
                    let punishChannel;
                    if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                    const target = tMember.user;
                    const staff = await client.fetchUser(punished.staffID);
                    if (!(staff && target)) return;

                    if (punishChannel) {
                        const unbanEmbed = new RichEmbed()
                            .setAuthor(`Moderation: Unmute [CaseID: ${caseID || "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                            .setColor("BLUE")
                            .setDescription(`**Punished By:** ${staff} (${staff.tag} | ${staff.id})\n**Punished User:** ${target} (${target.tag} | ${target.id})\n**Reason:** Punishment Expiry: "${punishment.reason}"`)
                            .setFooter(client.user.username, client.user.displayAvatarURL)
                            .setTimestamp();

                        punishChannel.send(unbanEmbed);
                    }
                    return punishment;
                }).catch(() => {
                    return false;
                });
            },
            /**
             * Re-Adds a potentially missing temporary mute, or received through warnings.
             * @param {Client} client - The Instance of the bot
             * @param {User || Integer} caseID - The Case ID to lookup
             * @returns {Boolean} Did it succeed?
             */
            temp: async (client, caseID) => {
                const punishment = client.getCase(caseID);
                if (!punishment) return false;

                const guild = client.guilds.get(punishment.guildID);
                const muteRule = guild.roles.find(r => r.name.toLowerCase().includes("mute"));
                if (!muteRule) return;

                const target = guild.members.get(punishment.punishedID);
                if (!target) return null;

                const punishChannelID = client.getGuildSetting(guild, "punishLog");
                let punishChannel;
                if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                let remaining = (punishment.date + punishment.length) - new Date().getTime();

                if (!target.roles.has(muteRule.id)) target.setRoles([muteRule.id], punishment.reason).then(async () => {
                    if (!punishChannel) return;
                    console.log("yeah?");
                    const muteEmbed = new RichEmbed()
                        .setAuthor(`Moderation: TempMute [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                        .setColor("BLUE")
                        .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Length:** ${convert(punishment.length, { long: true })}\n**Reason:** ${punishment.reason}`)
                        .setFooter(client.user.username, client.user.displayAvatarURL)
                        .setTimestamp();

                    return punishChannel.send(muteEmbed);
                }).catch((err) => { console.log(err); });

                setTimeout(async () => {
                    if (client.status === 5) return;
                    if ([4, 2].includes(client.status)) {
                        setTimeout(async () => {
                            if (![0, 1, 3].includes(client.status)) return;
                            await module.exports.auto.mute.remove(client, caseID);
                            if (punishChannel) {
                                const unmuteEmbed = new RichEmbed()
                                    .setAuthor(`Moderation: TempMute Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                    .setColor("BLUE")
                                    .setDescription(`**Punished By:** ${client.user}} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** TempMute Expired for "${punishment.reason}"`)
                                    .setFooter(client.user.username, client.user.displayAvatarURL)
                                    .setTimestamp();

                                punishChannel.send(unmuteEmbed);
                            }
                        }, 60000);
                    } else {
                        await module.exports.auto.mute.remove(client, caseID);
                        if (punishChannel) {
                            const unmuteEmbed = new RichEmbed()
                                .setAuthor(`Moderation: TempMute Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                                .setColor("BLUE")
                                .setDescription(`**Punished By:** ${client.user}} (${client.user.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** TempMute Expired for "${punishment.reason}"`)
                                .setFooter(client.user.username, client.user.displayAvatarURL)
                                .setTimestamp();

                            punishChannel.send(unmuteEmbed);
                        }
                    }
                }, remaining);
            }
        },
        ban: {
            /**
             * Unbans a User in the guild and expires the punishment.
             * @param {Client} client - The Instance of the bot.
             * @param {Integer} caseID - The Case ID to lookup.
             * @returns {Boolean} Did it succeed?
             */
            remove: async (client, caseID) => {
                const punishment = client.getCase(caseID);
                if (!punishment) return false;

                const guild = client.guilds.get(punishment.guildID);

                return await guild.fetchBan(punishment.punishedID).then(async ban => {
                    await guild.unban(ban.user, `Punishment Expiry: "${punishment.reason}"`).then(async () => {
                        if (!punishment.expired) client.expirePunishment(caseID);
                        client.removeTemp(caseID);

                        const punishChannelID = client.getGuildSetting(guild, "punishLog");
                        let punishChannel;
                        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                        const target = await client.fetchUser(punishment.punishedID).catch(() => {});
                        const staff = await client.fetchUser(punished.staffID).catch(() => {});;
                        if (!(staff && target)) return null;

                        if (punishChannel) {
                            const unbanEmbed = new RichEmbed()
                                .setAuthor(`Moderation: Unban [CaseID: ${caseID || "???"}] | ${target.tag}`, target.displayAvatarURL)
                                .setColor("BLUE")
                                .setDescription(`**Punished By:** ${staff} (${staff.tag} | ${staff.id})\n**Punished User:** ${target} (${target.tag} | ${target.id})\n**Reason:** Punishment Expiry: "${punishment.reason}"`)
                                .setFooter(client.user.username, client.user.displayAvatarURL)
                                .setTimestamp();

                            punishChannel.send(unbanEmbed);
                        }

                        return punishment;
                    }).catch(() => {
                        return false;
                    });
                }).catch(() => {
                    return false;
                });
            },
            /**
             * Re-Adds a potentially missing temporary ban, or received through warnings.
             * @param {Client} client - The Instance of the bot.
             * @param {Integer} caseID - The Case ID to lookup.
             * @returns {Boolean} Did it succeed?
             */
            temp: async (client, caseID) => {
                const punishment = client.getCase(caseID);
                if (!punishment) return false;

                const guild = client.guilds.get(punishment.guildID);

                const target = guild.members.get(punishment.punishedID);
                if (!target) return null;

                let remaining = (punishment.date + punishment.length) - new Date().getTime();

                guild.ban(target, { reason: `autoban: "${punishment.reason}"` }).then(async () => {

                    const punishChannelID = client.getGuildSetting(guild, "punishLog");
                    let punishChannel;
                    if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                    setTimeout(async () => {
                        if (client.status === 5) return;
                        if ([4, 2].includes(client.status)) {
                            setTimeout(async () => {
                                if (![0, 1, 3].includes(client.status)) return;
                                if (punishChannel) {
                                    const unbanEmbed = new RichEmbed()
                                        .setAuthor(`Moderation: TempBan Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                                        .setColor("BLUE")
                                        .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** Tempban Expired for "${punishment.reason}"`)
                                        .setFooter(client.user.username, client.user.displayAvatarURL)
                                        .setTimestamp();

                                    punishChannel.send(unbanEmbed);
                                }
                                return await module.exports.auto.ban.remove(client, caseID);
                            }, 60000);
                        } else {
                            if (punishChannel) {
                                const unbanEmbed = new RichEmbed()
                                    .setAuthor(`Moderation: TempBan Expired [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                                    .setColor("BLUE")
                                    .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** Tempban Expired for "${reason}"`)
                                    .setFooter(client.user.username, client.user.displayAvatarURL)
                                    .setTimestamp();

                                punishChannel.send(unbanEmbed);
                            }
                            return await module.exports.auto.ban.remove(client, caseID);
                        }
                    }, remaining);

                    if (!punishChannel) return;
                    const banEmbed = new RichEmbed()
                        .setAuthor(`Moderation: TempBan [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                        .setColor("BLUE")
                        .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** ${punishment.reason}`)
                        .setFooter(client.user.username, client.user.displayAvatarURL)
                        .setTimestamp();

                    return punishChannel.send(banEmbed);
                }).catch(() => {});
            },
            /**
             * Re-Adds a potentially missing ban, or received through warnings.
             * @param {Client} client - The Instance of the bot.
             * @param {Integer} caseID - The Case ID to lookup.
             * @returns {Boolean} Did it succeed?
             */
            add: async (client, caseID) => {
                const punishment = client.getCase(caseID);
                if (!punishment) return false;

                const guild = client.guilds.get(punishment.guildID);

                const target = guild.members.get(punishment.punishedID);
                if (!target) return null;

                guild.ban(target, { reason: `autoban: "${punishment.reason}"` }).then(async () => {
                    const punishChannelID = client.getGuildSetting(guild, "punishLog");
                    let punishChannel;
                    if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

                    if (!punishChannel) return;
                    const banEmbed = new RichEmbed()
                        .setAuthor(`Moderation: Ban [CaseID: ${caseID && caseID > 0 ? caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                        .setColor("BLUE")
                        .setDescription(`**Punished By:** ${client.user} (${client.user.tag})\n**Punished User:** ${target.user} (${target.user.tag})\n**Reason:** ${punishment.reason}`)
                        .setFooter(client.user.username, client.user.displayAvatarURL)
                        .setTimestamp();

                    return punishChannel.send(banEmbed);
                }).catch(() => {});
            }
        }
    }
}