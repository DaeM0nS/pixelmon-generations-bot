const { RichEmbed, Client, User, Message, Collection } = require("discord.js");
const { convert } = require("../utilities/time");
const punish = require("../utilities/punish");

String.prototype.trunc = function(n, useWordBoundary) {
    if (this.length <= n) return this;
    var subString = this.substr(0, n - 1);
    return (useWordBoundary ? subString.substr(0, subString.lastIndexOf(' ')) : subString) + "...";
};

//Spam Filters
const maxMentions = 4,
    maxBanMentions = 7,
    maxDupeMsg = 3,
    maxSpamMsg = 6,
    dupeInterval = 5000,
    spamInterval = 5000,
    maxCapsPercent = 0.7,
    maxEmoji = 7,
    autoMuteLevel = 3,
    emojiRegex = /([\uD800-\uDBFF][\uDC00-\uDFFF])/g,
    inviteRegex = /discord(?:app\.com\/invite|\.gg(?:\/invite)?)\/([\w-]{2,255})/gi,
    offenderInterval = 5000,
    offenders = new Collection();

/**
 * Check whether a user should be automuted
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message that was filtered
 * @param {User} user - The User to check
 * @returns {Boolean} Whether they were muted or not.
 */
const checkAutoMute = async (client, message, user) => {
    if (!offenders.has(user.id)) return;
    const offender = offenders.get(user.id);
    if ((offender.time + offenderInterval) < new Date().getTime()) offenders.delete(user.id);

    if (offender.infractions >= 3) {
        punish.mute.temp(client, message, user, convert("1h"), "Automute: 3+ infractions in 5 seconds.");
        return offenders.delete(user.id);
    }
}

/**
 * Check whether a user should be automuted
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message that was filtered
 * @param {User} user - The User to check
 * @returns {Boolean} Whether they were muted or not.
 */
const addInfraction = async (client, message, user) => {
    if (!offenders.has(user.id)) offenders.set(user.id, { amount: 0, time: new Date().getTime() });

    const infractions = offenders.get(user.id);
    if ((infractions.time + offenderInterval) < new Date().getTime())
        offenders.set(user.id, { amount: ++infractions.amount, time: new Date().getTime() });
    return checkAutoMute(client, message, user);
}


/**
 * Check if a GuildMember needs filtering
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const all = async (client, message) => {
    try {
        if (await spam(client, message)) return true;
        if (await dupe(client, message)) return true;
        if (await invite(client, message)) return true;
        if (await mentions(client, message)) return true;
        if (await emoji(client, message)) return true;
        if (await caps(client, message)) return true;
        if (await charSpam(client, message)) return true;

        return false;
    } catch (err) {
        console.log(err);
        return false;
    }
}


/**
 * Check if a GuildMember is spamming
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const spam = async (client, message) => {
    const userMsgs = message.channel.messages.filter(msg => msg.author.id === message.author.id && (message.createdAt - msg.createdAt) < spamInterval);
    if (userMsgs.size >= maxSpamMsg) {
        const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
        let punishChannel;
        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);
        const timeDiff = userMsgs.last().createdTimestamp - userMsgs.first().createdTimestamp;

        await message.channel.bulkDelete(userMsgs, true).then(messages => {
            if (messages.size < maxSpamMsg) return false;
            message.reply("Please don't spam!").then(msg => msg.delete(10000)).catch(() => {});

            if (punishChannel) {
                const spamEmbed = new RichEmbed()
                    .setAuthor(`Filter: Spam | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Spammed ${messages.size} messages in ${(timeDiff/1000).toFixed(2)} Seconds.`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(spamEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err);
            return false;
        });
    }
    return false;
}


/**
 * Check if a GuildMember is spamming the same message
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const dupe = async (client, message) => {
    const userMsgs = message.channel.messages.filter(msg => (msg.author.id === message.author.id) && ((message.createdAt - msg.createdAt) < dupeInterval) && (message.content === msg.content));

    if (userMsgs >= maxDupeMsg) {
        const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
        let punishChannel;
        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);
        const timeDiff = userMsgs.last().createdTimestamp - userMsgs.first().createdTimestamp;

        await message.channel.bulkDelete(userMsgs, true).then(messages => {
            if (messages.size < maxSpamMsg) return false;
            message.reply("Please don't send the same message repeatedly!").then(msg => msg.delete(10000)).catch(() => {});

            if (punishChannel) {
                const spamEmbed = new RichEmbed()
                    .setAuthor(`Filter: Duplicate messages | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Sending 3+ Duplicate Messages in ${(timeDiff/1000).toFixed(2)} Seconds.`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(spamEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err);
            return false;
        });
    }
    return false;
}


/**
 * Check if a GuildMember is advertising invites
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const invite = async (client, message) => {
    if (message.content.match(inviteRegex)) {
        message.delete().then(async () => {
            const invite = await client.fetchInvite(message.content.match(inviteRegex)).catch(() => {});

            if (!invite) return false;

            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            message.reply("Please don't advertise other Discord servers.").then((msg) => msg.delete(10000));

            if (punishChannel) {

                const inviteEmbed = new RichEmbed()
                    .setAuthor(`Filter: Invite link | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Sent an Invite link.\n**Link:** ${message.content.match(inviteRegex)}\n**Links to:** ${invite ? invite.guild.name: "Nowhere"}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(inviteEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        });
    } else return false;
}


/**
 * Check if a GuildMember is mass mentioning
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const mentions = async (client, message) => {
    const totalMentions = message.mentions.members.size + message.mentions.roles.size;

    if (totalMentions >= maxMentions) {
        message.delete().then(msg => {
            msg.channel.send("Don't mass mention members!").then(msg => msg.delete(10000));
        }).catch(() => {});

        const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
        let punishChannel;
        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

        const mentionEmbed = new RichEmbed()
            .setAuthor(`Filter: Mass Mentions | ${message.author.tag}`, message.author.displayAvatarURL)
            .setColor("BLUE")
            .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Mass Mentioning Users/Roles.\n**Amount mentioned:** ${totalMentions}`)
            .setFooter(client.user.username, client.user.displayAvatarURL)
            .setTimestamp();

        if (totalMentions >= maxBanMentions) {
            punish.ban.add(client, message, message.author, "Mass Mentioning (7+)");
        } else {
            punish.mute.temp(client, message, message.author, convert("1h"), "Mass Mentioning (4-6)");
        }
        punishChannel.send(mentionEmbed);
        return true;
    }
    return false;
}


/**
 * Check if a GuildMember is spamming emojis
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const emoji = async (client, message) => {
    const emojiAmount = message.content.match(emojiRegex) ? message.content.match(emojiRegex).length : 0;

    if (emojiAmount >= maxEmoji) {
        message.delete().then(async () => {
            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            message.reply("Please don't spam emojis.").then((msg) => msg.delete(10000));

            if (punishChannel) {

                const emojiEmbed = new RichEmbed()
                    .setAuthor(`Filter: Emoji Spam | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Spamming Emojis.`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(emojiEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err);
            return false;
        });
    } else return false;
}

/**
 * Check if a GuildMember is using excessive caps
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const caps = async (client, message) => {
    if (message.content.length > 5) return;
    const msgLength = message.content.replace(/[\s]/g, "").length;
    const capslength = message.content.replace(/[^A-Z]/g, "").length;

    const capsPercent = capslength / msgLength;
    if (capsPercent >= maxCapsPercent) {
        message.delete().then(async () => {
            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            message.reply("Please don't use excessive caps.").then((msg) => msg.delete(10000));

            if (punishChannel) {

                const capsEmbed = new RichEmbed()
                    .setAuthor(`Filter: Excessive Caps | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Excessive Caps (${capsPercent}% caps).`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(capsEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err);
            return false;
        });
    } else return false;
}

/**
 * Check if a GuildMember is character spamming
 * @param {Client} client - The Initialised Client
 * @param {Message} message - The Message being checked
 * @returns {Boolean}
 */
const charSpam = async (client, message) => {
    if (message.content.match(/(.)\1\1\1\1+/gi)) {
        message.delete().then(async () => {
            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            message.reply("Please don't use spam the same characters repeatedly.").then((msg) => msg.delete(10000));

            if (punishChannel) {

                const charSpamEmbed = new RichEmbed()
                    .setAuthor(`Filter: Character Spam | ${message.author.tag}`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Filtered User:** ${message.author.tag} (${message.author.id})\n**Reason:** Character Spam.`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(charSpamEmbed);
            }
            addInfraction(client, message, message.author);
            return true;
        }).catch((err) => {
            console.log(err);
            return false;
        });
    } else return false;
}

module.exports = {
    all,
    spam,
    dupe,
    invite,
    mentions,
    emoji,
    caps,
    charSpam
}