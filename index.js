const { Client, Collection } = require("discord.js");
const { token } = require("./utilities/config");
const client = new Client();
const fs = require("fs");

if (!fs.existsSync("./database")) fs.mkdirSync("./database");

["aliases", "commands"].forEach(x => client[x] = new Collection());
["command", "event"].forEach(x => require(`./handlers/${x}`)(client));

client.login(token);