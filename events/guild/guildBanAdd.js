const { RichEmbed } = require("discord.js");

module.exports = async (client, guild, user) => {
    if (user.bot) return;

    const guildSettings = client.getGuildSettings(guild);
    const chatLogsChannelID = guildSettings.chatLogsChannel;

    if (!chatLogsChannelID) return;
    const chatLogsChannel = guild.channels.get(chatLogsChannelID);

    if (!chatLogsChannel) return;

    const banEmbed = new RichEmbed()
        .setAuthor("Member Banned", user.displayAvatarURL)
        .setDescription(`${user} | ${user.tag} (${user.id})`)
        .setThumbnail(user.displayAvatarURL)
        .setColor("BLUE")
        .setTimestamp()
        .setFooter(`ID: ${user.id ? user.id : "?"}`, client.user.displayAvatarURL);

    return chatLogsChannel.send(banEmbed).catch(() => {});
}