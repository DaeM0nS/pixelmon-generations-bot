module.exports = async (client, guild) => {
    return console.log(`[Guild Leave] ${guild.name}\nOwner: ${guild.owner ? guild.owner.user.tag : "Unknown"}\nMembers: ${guild.members.size}`);
}