module.exports = async (client, guild) => {
    client.database.prepare("INSERT INTO PixelGensBot (guildID) VALUES (?)").run(guild.id);

    return console.log(`[Guild Join] ${guild.name}\nOwner: ${guild.owner ? guild.owner.user.tag : "Unknown"}\nMembers: ${guild.members.size}`);
}