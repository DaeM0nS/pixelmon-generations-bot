const { RichEmbed } = require("discord.js");
const filter = require("../../utilities/filter");

module.exports = async (client, message) => {
    if (message.author.bot) return;
    const prefix = client.prefix;

    //if (message.guild && !client.isStaff(message.member)) {
    //    if (await filter.all(client, message)) return;
    //}
	
    if (message.content.indexOf(prefix) !== 0) return;
    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const cmd = args.shift().toLowerCase();

    const commandFile = client.commands.get(cmd) || client.commands.get(client.aliases.get(cmd));
    if (commandFile) {
        try {
            if (commandFile.config.guildOnly === undefined) commandFile.config.guildOnly = true;
            if (!message.guild && commandFile.config.guildOnly) return message.channel.send('That command only works in guilds!');
            commandFile.exec(client, message, args);
        } catch (err) {
            console.log(err);
        }
    } else {
        if (!message.guild) return;
        const tag = client.getTag(message.guild, cmd.toLowerCase()) || client.getTagByAlias(message.guild, cmd.toLowerCase());
        if (!tag) return;

        if (tag.dm && client.isStaff(message.member) && message.mentions.members.first()) {
            message.mentions.members.forEach(member => {
                if (!tag.embed) member.send(`${message.author.tag} (${message.author.id}) sent you this tag!\n${tag.description}`);
                else member.send(new RichEmbed().setColor("GREEN").setAuthor(`${member.user.tag} (${member.user.id})`, message.author.displayAvatarURL).setDescription(`${message.author.tag} (${message.author.id}) sent you this tag!\n${tag.description}`));
            });
        } else if (tag.dm) {
            if (!tag.embed) message.author.send(`This Tag was sent in DMs to avoid spam!\n${tag.description}`);
            else message.author.send(new RichEmbed().setColor("GREEN").setAuthor(`${message.author.tag} (${message.author.id})`, message.author.displayAvatarURL).setDescription(`This Tag was sent in DMs to avoid spam!\n${tag.description}`));
        } else {
            if (!tag.embed) message.channel.send(tag.description);
            else message.channel.send(new RichEmbed().setColor("GREEN").setAuthor(`${message.author.tag} (${message.author.id})`, message.author.displayAvatarURL).setDescription(tag.description));
        }

        client.incrementTagUsage(message.guild, tag.name);
    }
}
