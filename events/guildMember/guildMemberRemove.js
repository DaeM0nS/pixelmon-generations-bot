module.exports = async (client, member) => {
    const guildSettings = client.getGuildSettings(member.guild);
    if (!guildSettings) return;
    if (!(guildSettings.leaveChannel && guildSettings.leaveMessage)) return;

    const leaveChannel = member.guild.channels.get(guildSettings.leaveChannel);
    if (!leaveChannel) return; 

    const message = guildSettings.leaveMessage
        .replace(/%player%/gi, member.user.tag)
        .replace(/%user%/gi, member.user.tag)
        .replace(/%server%/gi, member.guild.name)
        .replace(/%guild%/gi, member.guild.name);

    return leaveChannel.send(message.replace(/[|\\~_`*]/g, "\\$&")).catch(() => {});
}