const { RichEmbed } = require("discord.js");

String.prototype.trunc = function(n, useWordBoundary) {
    if (this.length <= n) return this;
    let subString = this.substr(0, n - 1);
    return `${(useWordBoundary ? subString.substr(0, subString.lastIndexOf(" ")) : subString)}...`;
}

module.exports = async (client, message) => {
    if (message.author.bot || message.channel.type !== "text") return;

    const guildSettings = client.getGuildSettings(message.guild);
    const ignoredChannels = guildSettings.ignoredChannels;
    const chatLogsChannelID = guildSettings.chatLogsChannel;

    if (!chatLogsChannelID) return;
    if (JSON.parse(ignoredChannels).includes(message.channel.id)) return;
    const chatLogsChannel = message.guild.channels.get(chatLogsChannelID);

    if (message.content) message.content = message.content.trunc(512, true).replace(/[\_\*\`\~]/gi, "\\$&");

    const deleteEmbed = new RichEmbed()
        .setAuthor(message.author.tag, message.author.displayAvatarURL)
        .setDescription(`A message made by ${message.author ? message.author : "\`?\`"} was deleted from ${message.channel ? message.channel : "\`?\`"}! [Jump to Channel](https://discordapp.com/channels/${message.guild.id}/${message.channel.id})`)
        .addField("Message Deleted: ", (message.content !== undefined ? `${message.content}` : "?"))
        .setColor("BLUE")
        .setTimestamp()
        .setFooter(`Author ID: ${message.author.id ? message.author.id : "?"}`, client.user.displayAvatarURL);

    return chatLogsChannel.send(deleteEmbed).catch(() => {});
}