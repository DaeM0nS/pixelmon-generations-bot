const { RichEmbed } = require("discord.js");

String.prototype.trunc = function(n, useWordBoundary) {
    if (this.length <= n) return this;
    let subString = this.substr(0, n - 1);
    return `${(useWordBoundary ? subString.substr(0, subString.lastIndexOf(" ")) : subString)}...`;
}

module.exports = async (client, oldMsg, newMsg) => {
    if (oldMsg.author.bot || oldMsg.channel.type !== "text") return;

    const guildSettings = client.getGuildSettings(oldMsg.guild);
    const ignoredChannels = guildSettings.ignoredChannels;
    const chatLogsChannelID = guildSettings.chatLogsChannel;

    if (!chatLogsChannelID) return;
    if (JSON.parse(ignoredChannels).includes(oldMsg.channel.id)) return;
    const chatLogsChannel = oldMsg.guild.channels.get(chatLogsChannelID);

    if (oldMsg.content) oldMsg.content = oldMsg.content.trunc(512, true).replace(/[\_\*\`\~]/gi, "\\$&");
    if (newMsg.content) newMsg.content = newMsg.content.trunc(512, true).replace(/[\_\*\`\~]/gi, "\\$&");

    const editEmbed = new RichEmbed()
        .setAuthor(oldMsg.author.tag, oldMsg.author.displayAvatarURL)
        .setDescription(`A message made by ${oldMsg.author ? oldMsg.author : "\`?\`"} was deleted from ${oldMsg.channel ? oldMsg.channel : "\`?\`"}! ${oldMsg.url ? `[View Message](${oldMsg.url})` : ""}`)
        .addField("Old Message:", oldMsg.content !== undefined ? oldMsg.content : "Unknown (Embed?)")
        .addField("New Message:", newMsg.content !== undefined ? newMsg.content : "Unknown (Embed?)")
        .setColor("BLUE")
        .setTimestamp()
        .setFooter(`Author ID: ${oldMsg.author.id ? oldMsg.author.id : "?"}\nMessage ID: ${oldMsg.id ? oldMsg.id: "?"}`, client.user.displayAvatarURL);

        return chatLogsChannel.send(editEmbed).catch(() => {});
}