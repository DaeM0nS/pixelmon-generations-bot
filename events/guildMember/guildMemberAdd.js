module.exports = async (client, member) => {
    const guildSettings = client.getGuildSettings(member.guild);
    if (!guildSettings) return;
    if (!(guildSettings.welcomeChannel && guildSettings.welcomeMessage)) return;

    const welcomeChannel = member.guild.channels.get(guildSettings.welcomeChannel);
    if (!welcomeChannel) return; 

    const message = guildSettings.welcomeMessage
        .replace(/%player%/gi, member.user.tag)
        .replace(/%user%/gi, member.user.tag)
        .replace(/%server%/gi, member.guild.name)
        .replace(/%guild%/gi, member.guild.name);

    return welcomeChannel.send(message.replace(/[|\\~_`*]/g, "\\$&")).catch(() => {});
}