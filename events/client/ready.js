const { prefix } = require(`../../utilities/config`);
const { readdirSync } = require("fs");

module.exports = async (client) => {
    console.log(`API connection successful.`);
    client.user.setActivity(`${prefix}help | Pixelmon Generations`, { type: 'WATCHING' });
    client.owner = await client.fetchUser('419327018962780170');
    client.database = require("../../utilities/SQLite/init.js")(client);
    readdirSync("./utilities/SQLite/schemas/").forEach(x => require(`../../utilities/SQLite/schemas/${x}`)(client));
    readdirSync("./utilities/SQLite/").filter(file => file.endsWith(".js")).forEach(x => require(`../../utilities/SQLite/${x}`)(client));

    setInterval(() => {
        try {
            client.database.checkpoint();
        } catch (e) {}
    }, 30000).unref();


    process.on('exit', () => client.database.close());

    await client.checkTemps();
    client.prefix = prefix;

    console.log("Bot successfully Initialised.");
}