<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for build-url, contributors-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Build Status][build-shield]][build-url]
[![Contributors][contributors-shield]][contributors-url]
[![MIT License][license-shield]][license-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/">
    <img src="https://cdn.discordapp.com/icons/336310801705205762/17d4248c4f26d78ec622ea8d78be86ae.png" alt="Logo" width="128" height="128">
  </a>

  <h3 align="center">Pixelmon Generations Discord Bot</h3>

  <p align="center">
    <br />
    ·
    <a href="https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project

This project is a Discord Bot created for use in any and all Pixelmon Generations' Discord servers. It provides functionalities such as forms of moderation for the Discords, with potential for the additions of future posibilities.

### Built With
* [discord.js](https://github.com/discordjs/discord.js)
* [better-sqlite3](https://github.com/JoshuaWise/better-sqlite3)
* [node-fetch](https://github.com/bitinn/node-fetch)

## License

Distributed under the MIT License. See [`license`][license-url] for more information.


<!-- CONTACT -->
## Contact

Your Name - [@Roguevashda](https://twitter.com/roguevashda)

Discord - [Roguevashda#0001](https://discord.gg/TD7SqmP)

Project Link: [https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/](https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[build-shield]: https://img.shields.io/badge/build-passing-brightgreen.svg
[build-url]: #
[contributors-shield]: https://img.shields.io/badge/contributors-1-orange.svg
[contributors-url]: https://gitlab.com/PixelmonGenerations/pixelmon-generations-bot/-/graphs/master
[license-shield]: https://img.shields.io/badge/License-AGPL%20v3-blue.svg
[license-url]: https://choosealicense.com/licenses/agpl-3.0