const { readdirSync } = require("fs")

module.exports = (client) => {
    const load = dir => {
        const commands = readdirSync(`./commands/${dir}/`).filter(f => f.endsWith('.js'));
        for (let file of commands) {
            let props = require(`../commands/${dir}/${file}`);
            client.commands.set(props.config.name, props);
            if (props.config.aliases) props.config.aliases.forEach(cmd => client.aliases.set(cmd, props.config.name.toLowerCase()));
		}
	}
    readdirSync('./commands/').forEach(x => load(x));
}