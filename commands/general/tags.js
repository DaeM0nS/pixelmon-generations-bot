const { RichEmbed, GuildMember } = require("discord.js");
const { error, success } = require("../../utilities/embeds");

const cleaned = (a) => {
    return a.replace(/`/g, `\`\u200b\``);
};

module.exports = {
    config: {
        name: "tag",
        aliases: ["tags"],
        type: "general",
        description: "View a list/information about tags, see their source, or for Admins, add, remove, and modify tags.",
        usage: "<Action> <Argument(s)>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        let subcmd;
        if (!args[0]) subcmd = "list";
        else subcmd = args[0].toLowerCase();

        if (!args[1] && ["add", "rename", "source", "new", "create", "rm", "del", "delete", "remove", "edit", "alias", "embed", "dm", "type", "info", "category"].includes(args[0])) return error(message, "Please specify one or more arguments!");
        if (!args[2] && ["add", "new", "rename", "create", "edit", "alias"].includes(args[0])) return error(message, "Please specify two or more arguments!");

        switch (subcmd) {
            case "add":
            case "create":
            case "new":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const newTag = client.createTag(message.guild, args[1].toLowerCase(), args.slice(2).join(" "), message.author, false, null, null);
                if (!newTag || newTag.lastInsertRowid < 1) return error(message, "Either that tag already exists, or a command has that name!");
                else success(message, `Successfully added the \`${args[1]}\` tag!`, false);
                break;
            case "rename":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const tagToRename = client.getTag(message.guild, args[1]);
                if (!tagToRename) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                const newTagName = client.getTag(message.guild, args.slice(2).join('-'));
                if (newTagName) return error(message, "A tag already exists by that name!");

                const renamedTag = client.renameTag(message.guild, args[1], args.slice(2).join('-'), message.member);

                if (!renamedTag) return error(message, "Couldn't rename tag.");
                else success(message, `Successfully renamed \`${args[1]}\`to \`${args.slice(2).join('-')}\`!`, false);
                break;
            case "source":
            case "raw":
                const rawTag = client.getTag(message.guild, args[1]);
                if (!rawTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                else success(message, `\`\`\`\n${cleaned(rawTag.description)}\`\`\``, false);
                break;
            case "rm":
            case "remove":
            case "del":
            case "delete":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const delTag = client.getTag(message.guild, args[1]);
                if (!delTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                client.deleteTag(message.guild, args[1]);
                success(message, `Successfully deleted the tag \`${args[1]}\``, false);
                break;
            case "info":
                const tagInfo = client.getTag(message.guild, args[1]) || client.getTagByAlias(message.guild, args[1]);
                if (!tagInfo) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);

                const tagAuthor = await client.fetchUser(`${tagInfo.addedBy}`).catch(() => {});

                const tagInfoEmbed = new RichEmbed()
                    .setAuthor(`${message.author.tag} (${message.author.id})`, message.author.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Name**: ${tagInfo.name}\n**Aliases**: ${JSON.parse(tagInfo.aliases).length ? JSON.parse(tagInfo.aliases).map(a => `\`${a}\``).join(" ") : "None"}\n**Category**: ${tagInfo.category ? tagInfo.category.split(' ').map(str => str.slice(0, 1).toUpperCase() + str.slice(1)).join(' ') : "No category"}\n**Type**: ${tagInfo.embed ? "Embed" : "Message"}\n**Added By**: ${tagAuthor ? `${tagAuthor.tag} (${tagAuthor.id})` : tagInfo.addedBy}\n**Uses**: ${tagInfo.uses}`)
                    .setFooter("Added At ")
                    .setTimestamp(tagInfo.addedAt);

                message.channel.send(tagInfoEmbed);
                break;
            case "edit":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const tagEdit = client.getTag(message.guild, args[1]);
                if (!tagEdit) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                const editedTag = client.updateTag(message.guild, args[1], args.slice(2).join(' '));
                if (!editedTag) return error(message, "I couldn't edit the tag's message!");
                success(message, `Successfully edited the tag \`${args[1]}\`.`, false);
                break;
            case "alias":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                if (client.commands.has() || client.aliases.has()) return error(message, "A command is already using that!");
                if (client.database.prepare("SELECT * FROM tags WHERE name = ? AND guildId = ?;").get(args[2], message.guild.id) || client.database.prepare("SELECT * FROM tags WHERE aliases LIKE ? AND guildId = ?;").get(`%"${args[2]}"%`, message.guild.id)) {
                    return error(message, "That alias is already in use by another tag!");
                }
                const aliasTag = client.getTag(message.guild, args[1]);
                if (!aliasTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                client.addAlias(message.guild, args[1], args.slice(2).join('-'));
                success(message, `Successfully added the alias \`${args.slice(2).join('-')}\` to the tag \`${args[1]}\``, false);
                break;
            default:
            case "list":
                const tagList = client.getAllTags(message.guild);
                const listEmbed = new RichEmbed()
                    .setAuthor(message.author.tag, message.author.displayAvatarURL)
                    .setFooter(client.user.tag, client.user.displayAvatarURL)
                    .setTimestamp();
                if (!tagList || tagList.length < 1) {
                    listEmbed.setColor("RED");
                    listEmbed.setDescription("This guild doesn't have any tags.");
                } else {
                    listEmbed.setColor("BLUE");
                    if (tagList.filter((tag) => !tag.category).length) listEmbed.setDescription(tagList.filter((tag) => !tag.category).map(tag => `\`${tag.name}\``).join(' '));
                    let tags = tagList.filter((tag) => tag.category);
                    const categories = [];
                    tags.map(tag => tag.category.toLowerCase()).forEach((category) => {
                        if (categories.some((cat) => cat === category)) return;
                        categories.push(category);
                    });
                    categories.forEach((category) => {
                        const thisCatTags = tags.filter((tag) => tag.category === category);
                        listEmbed.addField(category.split(' ').map(str => str.slice(0, 1).toUpperCase() + str.slice(1)).join(' '), thisCatTags.map(tag => `\`${tag.name}\``).join(' '));
                    });
                }
                message.channel.send(listEmbed);
                break;
            case "embed":
            case "type":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const embedTag = client.getTag(message.guild, args[1]);
                if (!embedTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                if (client.tagEmbed(message.guild, args[1], !embedTag.embed)) return success(message, `Successfully set the tag \`${args[1]}\` to ${!embedTag.embed ? "an Embed" : "a Message"}`, false);
                error(message, "Unable to set tag type.");
                break;
            case "dm":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const dmTag = client.getTag(message.guild, args[1]);
                if (!dmTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                if (client.tagEmbed(message.guild, args[1], !dmTag.dm)) return success(message, `Successfully set the tag \`${args[1]}\` to ${!dmTag.dm ? "" : "not"} be DMable`, false);
                error(message, "Unable to set tag type.");
                break;
            case "category":
                if (!isAdmin(message.member)) return error(message, "You do not have permission for that.");
                const catTag = client.getTag(message.guild, args[1]);
                if (!catTag) return error(message, `I couldn't find a tag by the name \`${args[1]}\``);
                if (client.tagCategory(message.guild, args[1], args.slice(2).join(' '))) return success(message, `Successfully set the Category for \`${args[1]}\` to \`${args.slice(2).join(' ')}\``, false);
                error(message, "Unable to set tag category.");
                break;
        }

    }
}

/**
 * Checks if the member is an Admin
 * @param {GuildMember} member - The Member to check
 * @returns {boolean}
 */
function isAdmin(member) {
    return member.hasPermission(["ADMINISTRATOR", "MANAGE_GUILD"]);
}