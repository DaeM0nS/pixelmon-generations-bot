const { RichEmbed } = require('discord.js');
const { readdirSync } = require('fs');

module.exports = {
    config: {
        name: "help",
        aliases: ["commands"],
        type: "general",
        description: "Displays help with commands, or a list of commands.",
        usage: "[command]",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        const embed = new RichEmbed()
            .setAuthor(`${client.user.username}'s Help`, client.user.displayAvatarURL)
            .setThumbnail(client.user.displayAvatarURL)
            .setTimestamp()
            .setColor("BLUE");

        if (args[0]) {
            const command = client.commands.get(args[0].toLowerCase()) ? client.commands.get(args[0].toLowerCase()).config : client.commands.get(client.aliases.get(args[0].toLowerCase())).config;
            let string = `**❯ Name**: ${command.name.slice(0,1).toUpperCase() + command.name.slice(1)}\n`;
            if (command.type) string += `**❯ Category**: ${command.type}\n`;
            if (command.description) string += `**❯ Description:**  ${command.description}\n`;
            string += `**❯ Usage:** \`${client.prefix}${command.name} ${command.usage || ""}\`\n`;
            if (command.aliases.length > 0) string += `**❯ Aliases:** ${command.aliases.join(', ')}\n`;
            embed.setDescription(string)

            return message.channel.send(embed);
        } else {
            const categories = readdirSync('./commands/');
            embed.setDescription(`${client.user.username} is designed and maintained for use in all Pixelmon Generations' discords. Some commands may require arguments (**required - <>, optional - ()**).`);
            embed.setFooter(`${client.user.username} | Total Commands: ${client.commands.size}`, client.user.displayAvatarURL);

            categories.forEach((category) => {
                const thisCategory = client.commands.filter(c => c.config.type.toLowerCase() === category.toLowerCase());
                const capitalise = category.slice(0, 1).toUpperCase() + category.slice(1);
                try {
                    embed.addField(`❯ ${capitalise} [${thisCategory.size}]:`, thisCategory.map(c => `\`${c.config.name}\``).join(' '));
                } catch (e) {
                    console.log(e);
                }
            });

            return message.channel.send(embed);
        }
    }
}