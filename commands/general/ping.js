const fetch = require('node-fetch');

module.exports = {
    config: {
        name: "ping",
        aliases: [],
        type: "general",
        description: "PONG! returns the bots ping and api ping",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        new Promise((resolve, reject) => {
            const start = Date.now();
            fetch(`https://discordapp.com/api/v6/channels/${message.channel.id}/typing`, {
                method: "post",
                headers: {
                    "Authorization": `Bot ${client.token}`
                }
            }).then(() => {
                const time = Date.now() - start;
                resolve(time);
                message.channel.send(`**Pong!** Bot Latency: \`${time}ms\`, API Latency: \`${Math.round(client.ping)}ms\``);
            }).catch(err => {
                reject(err);
            });
        });
    }
}
