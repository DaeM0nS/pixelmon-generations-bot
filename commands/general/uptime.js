const { RichEmbed } = require("discord.js");
const time = require("../../utilities/time");

module.exports = {
    config: {
        name: "uptime",
        aliases: ['clientuptime', 'botuptime'],
        type: "general",
        description: "Uptime of the bot",
        usage: "uptime",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        return message.channel.send(new RichEmbed().setColor("BLUE").setDescription(`**Uptime:** ${time.toString(client.uptime)}`));
    }
}


