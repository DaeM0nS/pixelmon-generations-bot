const resolveRole = require("../../utilities/resolveRole");
const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "delstaffrole",
        aliases: ["removestaffrole", "removestaff"],
        type: "administration",
        description: "Removes a role from the list of staff.",
        usage: "<Role ID | @Role | Role Name>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        if (!(args[0] && message.mentions.roles.first())) {
            return error(message, `Incorrect Usage.\nUsage: ${client.prefix}${module.exports.config.name} ${module.exports.config.usage}`);
        }

        const role = resolveRole.single(args.join(" "), message.guild.channels);

        if (!role) {
            return error(message, `I couldn't find a role with ${args.join(" ")}`);
        }
        
        const roles = JSON.parse(client.getGuildSetting(message.guild, "staffRoles"));
        if (!roles.includes(role.id)) return error(message, "That role's not a staff role!");

        if (roles.indexOf(role.id) !== -1) roles.splice(roles.indexOf(role.id), 1);
        const changed = client.updateGuildSettings(message.guild, "staffRoles", JSON.stringify(roles));
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't remove role from the staff roles");
        }
        
        return success(message, `Removed ${role} from the list of staff roles!`);
    }
}