const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "setleavemessage",
        aliases: ["setgoodbyemessage"],
        type: "administration",
        description: "Changes the leave message. Empty = none",
        usage: "[message]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        let leaveMessage = args[0] ? args.join(" ") : null;
        
        const changed = client.updateGuildSettings(message.guild, "leaveMessage", leaveMessage);
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't change the Leave Message");
        }
        
        return success(message, `Set the Leave Message to \`\`\`\n${leaveMessage}\n\`\`\``);
    }
}