const resolveChannel = require("../../utilities/resolveChannel");
const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "setchatlogschannel",
        aliases: ["setchatlogs", "setlogschannel"],
        type: "administration",
        description: "Set the channel chat messages (edit/delete) are logged to.",
        usage: "<Channel ID | #Channel | Channel Name>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        if (!(args[0] && message.mentions.channels.first())) {
            return error(message, `Incorrect Usage.\nUsage: ${client.prefix}${module.exports.config.name} ${module.exports.config.usage}`);
        }

        const channel = resolveChannel(args.join(" "), message.guild.channels);

        if (!channel) {
            return error(message, `I couldn't find a channel with ${args.join(" ")}`);
        }

        if (!channel.permissionsFor(message.guild.me).has(["VIEW_CHANNEL", "SEND_MESSAGES"])) {
            return error(message, "I can't view or send messages to that channel!");
        }
        
        const changed = client.updateGuildSettings(message.guild, "chatLogsChannel", channel.id);
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't change the Chat Logs Channel");
        }
        
        return success(message, `Set the Chat Logs channel to ${channel}`);
    }
}