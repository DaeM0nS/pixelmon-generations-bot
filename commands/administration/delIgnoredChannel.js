const resolveChannel = require("../../utilities/resolveChannel");
const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "delstaffrole",
        aliases: ["removestaffrole", "removestaff"],
        type: "administration",
        description: "Removes a role from the list of staff.",
        usage: "<Role ID | @Role | Role Name>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        if (!(args[0] && message.mentions.channels.first())) {
            return error(message, `Incorrect Usage.\nUsage: ${client.prefix}${module.exports.config.name} ${module.exports.config.usage}`);
        }

        const channel = resolveChannel(args.join(" "), message.guild.channels);

        if (!channel) {
            return error(message, `I couldn't find a channel with ${args.join(" ")}`);
        }
        
        const channels = JSON.parse(client.getGuildSetting(message.guild, "ignoredChannels"));
        if (!channels.includes(channel.id)) return error(message, "That channel's not being ignored!");

        if (channels.indexOf(channel.id) !== -1) channels.splice(channels.indexOf(channel.id), 1);
        const changed = client.updateGuildSettings(message.guild, "ignoredChannels", JSON.stringify(channels));
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't unignore channel!");
        }
        
        return success(message, `Removed ${channel} from the list of ignored channels!`);
    }
}