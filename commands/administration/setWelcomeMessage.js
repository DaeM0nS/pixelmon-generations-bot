const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "setwelcomemessage",
        aliases: ["sethellomessage"],
        type: "Administration",
        description: "Changes the welcome message. Empty = none",
        usage: "[message]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        const welcomeMessage = args.join(" ");
        
        const changed = client.updateGuildSettings(message.guild, "welcomeMessage", welcomeMessage ? welcomeMessage : "null");
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't change the Welcome Message");
        }
        
        return success(message, `Set the Welcome Message to \`\`\`\n${welcomeMessage}\n\`\`\``);
    }
}