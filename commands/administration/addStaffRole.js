const { resolveRole } = require("../../utilities/resolveRole");
const { error, success } = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "addstaffrole",
        aliases: ["addstaff"],
        type: "administration",
        description: "Adds a role to the list of staff.",
        usage: "<Role ID | @Role | Role Name>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["ADMINISTRATOR", "MANAGE_SERVER"])) {
            return error(message, "Invalid Permissions. You need either `ADMINISTRATOR` or `MANAGE_SERVER` to use this command.");
        }

        if (!(args[0] || message.mentions.roles.first())) {
            return error(message, `Incorrect Usage.\nUsage: ${client.prefix}${module.exports.config.name} ${module.exports.config.usage}`);
        }

        const role = resolveRole(args.join(" "), message.guild.roles);

        if (!role) {
            return error(message, `I couldn't find a role with ${args.join(" ")}`);
        }
        
        const roles = JSON.parse(client.getGuildSetting(message.guild, "staffRoles"));
        if (roles.includes(role.id)) return error(message, "That role's already a staff role!");

        roles.push(role.id);
        const changed = client.updateGuildSettings(message.guild, "staffRoles", JSON.stringify(roles));
        
        if (!changed || changed < 0) {
            return error(message, "Couldn't add role to the staff roles");
        }
        
        return success(message, `Added ${role} to the list of staff roles!`);
    }
}