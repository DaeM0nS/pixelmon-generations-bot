const { readdirSync } = require('fs');
const { join } = require('path');

module.exports = {
    config: {
        name: "reload",
        aliases: [],
        type: "owner",
        description: "Reloads a command.",
        usage: "<command>",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        if (message.author !== client.owner) return;

        if (!args[0]) return message.channel.send("Please provide a command to reload!");
        const commandName = args[0].toLowerCase();
        if (!client.commands.get(commandName)) return message.channel.send('That command doesnt exist. Try again.');

        readdirSync(join(__dirname, '..')).forEach(f => {
            let files = readdirSync(join(__dirname, '..', f));
            if (files.includes(commandName + '.js')) {
                try {
                    delete require.cache[require.resolve(join(__dirname, '..', f, `${commandName}.js`))];
                    client.commands.delete(commandName);
                    const pull = require(`../${f}/${commandName}.js`);
                    client.commands.set(commandName, pull);
                    return message.channel.send(`Successfully reloaded ${commandName}.js!`);
                } catch (e) {
                    return message.channel.send(`Could not reload: \`${args[0].toUpperCase()}\``);
                }
            }
        });
    }
}