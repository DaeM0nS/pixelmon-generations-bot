const { RichEmbed } = require("discord.js");

module.exports = {
    config: {
        name: "shutdown",
        aliases: ["stop"],
        type: "owner",
        description: "Shuts the bot down.",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        if (message.author !== client.owner) return;
        return message.channel.send(new RichEmbed().setColor("BLUE").setDescription("✅ Shutting down."))
            .then(async () => {
                await client.database.checkpoint();
                await client.database.close();
                await client.destroy();
                await process.exit(0);
            });
    }
}