const { readdirSync, unlinkSync } = require("fs");

module.exports = {
    config: {
        name: "regendatabase",
        aliases: ["regendb"],
        type: "owner",
        description: "A Test Command",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (message.author !== client.owner) return;

        await client.database.close();
        await unlinkSync(client.database.name)
        await message.channel.send("Deleted Database").then(async msg => {
            client.database = await (require("../../utilities/SQLite/init.js")(client));
            await (readdirSync("./utilities/SQLite/schemas/").forEach(x => require(`../../utilities/SQLite/schemas/${x}`)(client)));
            await client.database.prepare("INSERT INTO PixelGensBot (guildID) VALUES (?)").run(message.guild.id);
            await msg.edit("Done.");
            await msg.react('✅');
        });
    }
}