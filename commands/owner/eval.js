const Discord = require("discord.js");
const beautify = require("beautify");

String.prototype.trunc = function(n, useWordBoundary) {
    if (this.length <= n) return this;
    let subString = this.substr(0, n - 1);
    return (useWordBoundary ? subString.substr(0, subString.lastIndexOf(' ')) : subString) + "...";
};

module.exports = {
    config: {
        name: "eval",
        aliases: ["e", "val"],
        type: "owner",
        description: "Evaluates code.",
        usage: "<to Evaluate>",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        if (message.author !== client.owner) return;

        let evalEmbed = new Discord.RichEmbed()
            .setTitle("Eval")
            .setFooter(client.user.tag, client.user.displayAvatarURL)
            .setTimestamp();

        let toEval = args.join(" ");
        try {
            if (!toEval) throw new Error("Evaluating nothing results in nothing.");
            let evaluated = await eval(toEval);

            let hrStart = process.hrtime();
            let hrDiff = process.hrtime(hrStart);

            evalEmbed.addField("To Evaluate", `\`\`\`js\n${beautify(toEval, { format: "js" }).trunc(1020, true)}\n\`\`\``);
            evalEmbed.addField("Evaluated", evaluated);
            evalEmbed.addField("\u200B", `**Type:** ${(typeof evaluated).replace(/\w\S+/g, function(word) { return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase(); })}\n**Time Taken:** *${hrDiff[0] > 0 ? `${hrDiff[0]}s ` : ''}${hrDiff[1] / 1000000}ms.*`);
            evalEmbed.setColor("GREEN");
        } catch (err) {
            evalEmbed.addField("Error", `\`\`\`js\n${err.message.trunc(1020, true)}\n\`\`\``);
            if (toEval) evalEmbed.addField("To evaluate:", `\`\`\`js\n${beautify(toEval, { format: "js" }).trunc(1020, true)}\n\`\`\``);
            evalEmbed.setColor("RED");
        }
        return message.channel.send(evalEmbed);
    }
}