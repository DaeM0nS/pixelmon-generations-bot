const { RichEmbed } = require("discord.js");
const embeds = require("../../utilities/embeds");
const { ban } = require("../../utilities/punish");

module.exports = {
    config: {
        name: "unban",
        aliases: [],
        type: "moderation",
        description: "Unbans a Member or User from the Discord.",
        usage: "<@Member | User Tag | MemberID> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["BAN_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = await client.fetchUser(args[0]);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        const banInfo = message.guild.fetchBan(target).catch(() => {
            return embeds.error(message, "Unable to find ban information for that user.");
        });
        if (!banInfo) return embeds.error(message, "Unable to find ban information for that user.");

        const reason = args.slice(1).join(" ") || "None";

        const removed = await ban.remove(client, target, "unknown");

        if (removed) {
            const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
            let punishChannel;
            if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

            embeds.success(message, "Successfully unbanned that user!");
            if (punishChannel) {
                const unbanEmbed = new RichEmbed()
                    .setAuthor(`Moderation: Unban [CaseID: ${removed.caseID && removed.caseID > 0 ? removed.caseID : "???"}] | ${target.tag}`, target.displayAvatarURL)
                    .setColor("BLUE")
                    .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.tag})\n**Reason:** ${reason}`)
                    .setFooter(client.user.username, client.user.displayAvatarURL)
                    .setTimestamp();

                punishChannel.send(unbanEmbed);
            }
        } else {
            embeds.error(message, "Unable to unban that user.");
        }
    }
}