const { RichEmbed } = require("discord.js");
const { mute } = require("../../utilities/punish");
const resolveMember = require("../../utilities/resolveMember");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "unmute",
        aliases: [],
        type: "moderation",
        description: "Unmute a Member or User in the Discord.",
        usage: "<@Member | User Tag | MemberID> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["KICK_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = await resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        const muteRule = message.guild.roles.find(r => r.name.toLowerCase().includes("mute"));
        if (!muteRule) return embeds.error(message, "Unable to find the mute role named `mute` or `muted`.");

        const hasRole = message.member.roles.has(muteRule.id);
        if (!hasRole) return embeds.error(message, "They don't have the mute role, were they unmuted manually?");

        const punishChannelID = client.getGuildSetting(message.guild, "punishLog");
        let punishChannel;
        if (punishChannelID) punishChannel = client.channels.get(punishChannelID);

        let punishment = client.getUnknownUnexpiredPunishment(target.user);
        if (!punishment || punishment.length > 1) punishment = client.getUnknownPunishment(target.user);
        await (mute.remove(client, target.user, "unknown"));
        
        embeds.success(message, "Successfully unmuted that user!");
        if (punishChannel) {
            const unbanEmbed = new RichEmbed()
                .setAuthor(`Moderation: Unmute [CaseID: ${punishment.caseID && punishment.caseID > 0 ? punishment.caseID : "???"}] | ${target.user.tag}`, target.user.displayAvatarURL)
                .setColor("BLUE")
                .setDescription(`**Punished By:** ${message.author} (${message.author.tag})\n**Punished User:** ${target} (${target.user.tag})\n**Reason:** ${args.slice(1).join(" ") || "None"}`)
                .setFooter(client.user.username, client.user.displayAvatarURL)
                .setTimestamp();

            punishChannel.send(unbanEmbed);
        }
    }
}