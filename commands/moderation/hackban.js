const embeds = require("../../utilities/embeds");
const { ban } = require("../../utilities/punish");

module.exports = {
    config: {
        name: "hackban",
        aliases: [],
        type: "moderation",
        description: "Bans a Member before they join the Discord.",
        usage: "<User ID> <Reason>",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["MANAGE_SERVER"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = await client.fetchUser(args[0]).catch(() => {});
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        const banInfo = await message.guild.fetchBan(target).catch(() => {});
        if (banInfo) return embeds.error(message, "That user is already banned!");

        const reason = args.slice(1).join(" ");
        if (!reason) return embeds.error(message, "You must provide a reason for this command!");
        return ban.hack(client, message, target, reason);
    }
}