const { RichEmbed } = require("discord.js");
const resolveChannel = require("../../utilities/resolveChannel");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "purge",
        aliases: [],
        type: "moderation",
        description: "Cleanses X messages in the channel.",
        usage: "<#Channel | Channel Name | Channel ID> <Amount> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        if (!message.member.hasPermission(["MANAGE_MESSAGES"])) return embeds.error(message, "You don't have permission to do that!", true);

        let deleteAmt;
        let reason;

        let channel = resolveChannel(args[0] ? args[0] : "", message.guild.channels);
        if (!channel) {
            deleteAmt = parseInt(args[0]);
            reason = args.slice(1).join(" ") || "No reason";
            channel = message.channel;
        } else {
            deleteAmt = parseInt(args[1]);
            reason = args.slice(2).join(" ") || "No reason";
        }

        if (!deleteAmt || deleteAmt === NaN) return embeds.error(message, "You must provide a valid number of messages to clean!");
        else if (deleteAmt < 1 || deleteAmt > 100) return embeds.error(message, "You can only provide a number between 1 and 100 inclusive.");

        
        message.delete().then(() => {
            message.channel.bulkDelete(deleteAmt, true).then(async messages => {
                const guildSettings = client.getGuildSettings(message.guild);
                const ignoredChannels = guildSettings.ignoredChannels;
                const chatLogsChannelID = guildSettings.chatLogsChannel;

                embeds.success(message, `Purged ${messages.size} messages!`);
        
                if (!chatLogsChannelID) return;
                if (JSON.parse(ignoredChannels).includes(message.channel.id)) return;
                const chatLogsChannel = message.guild.channels.get(chatLogsChannelID);
        
                if (chatLogsChannel) {
                    const purgeEmbed = new RichEmbed()
                        .setAuthor(`Moderation: Purge | ${message.author.tag}`, message.author.displayAvatarURL)
                        .setColor("BLUE")
                        .setDescription(`**Purged By:** ${message.author} (${message.author.tag})\n**Purged Channel:** ${channel} ([Jump to Channel](https://discordapp.com/channels/${channel.guild.id}/${channel.id}))\n**Messages Purged: ** ${messages.size}\n**Reason:** ${reason}`)
                        .setFooter(client.user.username, client.user.displayAvatarURL)
                        .setTimestamp();
        
                    chatLogsChannel.send(purgeEmbed);
                }
            });
        }).catch((err) => {
            return embeds.error(message, "Unable to purge channel: " + err.message);
        });
    }
}