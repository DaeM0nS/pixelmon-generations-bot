const { ban } = require("../../utilities/punish");
const resolveMember = require("../../utilities/resolveMember");
const { convert } = require("../../utilities/time");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "tempban",
        aliases: [],
        type: "moderation",
        description: "Temporarily bans a Member from the Discord.",
        usage: "<@Member | User Tag | MemberID> <Length> [Reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["BAN_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        if ((target.highestRole.position >= message.member.highestRole.position) && (!message.member.hasPermission(["ADMINISTRATOR"]) && !target.hasPermission(["ADMINISTRATOR"])) || message.guild.owner == target) {
            return embeds.error(message, "You can't target that person!");
        }

        if (!target.bannable) return embeds.error(message, "I can't target that person!");

        const time = convert(args[1]);
        if (!time || time < 1 || time === Infinity || time === NaN) return embeds.error(message, `You must provide an Int32 time argument. If you wish for a permanent ban, use ${client.prefix}ban`);

        const reason = args.slice(2).join(" ") || "No reason";

        ban.temp(client, message, target.user, time, reason);
    }
}