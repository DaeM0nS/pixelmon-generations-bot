const embeds = require("../../utilities/embeds");
const resolveMember = require("../../utilities/resolveMember");
const { kick } = require("../../utilities/punish");

module.exports = {
    config: {
        name: "kick",
        aliases: [],
        type: "moderation",
        description: "Kicks a Member from the Discord.",
        usage: "<@Member | User#Tag | MemberID> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["KICK_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        if ((target.highestRole.position >= message.member.highestRole.position) && (!message.member.hasPermission(["ADMINISTRATOR"]) && !target.hasPermission(["ADMINISTRATOR"])) || message.guild.owner == target) return embeds.error(message, "You can't kick that member!", true);
        if (!target.kickable) return embeds.error(message, "I can't kick that member!", true);

        const reason = args.slice(1).join(" ");
        return kick(client, message, target, reason);
    }
}