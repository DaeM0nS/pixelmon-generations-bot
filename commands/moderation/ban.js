const { ban } = require("../../utilities/punish");
const resolveMember = require("../../utilities/resolveMember");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "ban",
        aliases: [],
        type: "moderation",
        description: "Bans a Member from the Discord.",
        usage: "<@Member | User Tag | MemberID> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["BAN_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        if ((target.highestRole.position >= message.member.highestRole.position) && (!message.member.hasPermission(["ADMINISTRATOR"]) && !target.hasPermission(["ADMINISTRATOR"])) || message.guild.owner == target) {
            return embeds.error(message, "You can't target that person!");
        }

        if (!target.bannable) return embeds.error(message, "I can't target that person!");

        const reason = args.slice(1).join(" ") || "No reason";

        ban.add(client, message, target.user, reason);
    }
}