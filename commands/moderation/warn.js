const { warn } = require("../../utilities/punish");
const resolveMember = require("../../utilities/resolveMember");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "warn",
        aliases: [],
        type: "moderation",
        description: "Warns a Member.",
        usage: "<@Member | User Tag | MemberID> <reason>",
        guildOnly: false,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["MANAGE_MESSAGES"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        if ((target.highestRole.position >= message.member.highestRole.position) && (!message.member.hasPermission(["ADMINISTRATOR"]) && !target.hasPermission(["ADMINISTRATOR"])) || message.guild.owner == target) {
            return embeds.error(message, "You can't target that person!");
        }

        const reason = args.slice(1).join(" ");
        if (!reason) return embeds.error(message, "You must provide a reason for warning them!");

        warn(client, message, target, reason);
    }
}