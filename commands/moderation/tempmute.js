const { mute } = require("../../utilities/punish");
const resolveMember = require("../../utilities/resolveMember");
const { convert } = require("../../utilities/time");
const embeds = require("../../utilities/embeds");

module.exports = {
    config: {
        name: "tempmute",
        aliases: [],
        type: "moderation",
        description: "Temporarily mutes a Member.",
        usage: "<@Member | User Tag | MemberID> <Length> [reason]",
        guildOnly: true,
    },
    exec: async (client, message, args) => {
        message.delete(15000).catch(() => {});
        if (!message.member.hasPermission(["KICK_MEMBERS"])) return embeds.error(message, "You don't have permission to do that!", true);

        const target = resolveMember(args[0], message.guild.members);
        if (!target) return embeds.error(message, "Please target a valid person!", true);

        if ((target.highestRole.position >= message.member.highestRole.position) && (!message.member.hasPermission(["ADMINISTRATOR"]) && !target.hasPermission(["ADMINISTRATOR"])) || message.guild.owner == target) {
            return embeds.error(message, "You can't target that person!");
        }

        const time = convert(args[1]);
        if (!time || time < 1 || time === Infinity || time === NaN) return embeds.error(message, `You must provide an Int32 time argument. If you wish for a permanent mute, use ${client.prefix}mute`);

        const reason = args.slice(2).join(" ") || "No reason";

        mute.temp(client, message, target.user, time, reason);
    }
}